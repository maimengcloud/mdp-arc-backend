package com.mdp;


import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

//@SpringBootApplication
@SpringCloudApplication
//@EnableSwagger2
public class ArcApplication   {
	
	
	public static void main(String[] args) {
		SpringApplication.run(ArcApplication.class,args);

	}


}
