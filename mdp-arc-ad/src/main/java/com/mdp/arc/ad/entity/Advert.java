package  com.mdp.arc.ad.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 ad  小模块 Advert<br>
 */
@Data
@TableName("arc_ad_advert")
@ApiModel(description="广告")
public class Advert  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="广告位置",allowEmptyValue=true,example="",allowableValues="")
	String positionId;

	
	@ApiModelProperty(notes="媒体类型:0",allowEmptyValue=true,example="",allowableValues="")
	String mediaType;

	
	@ApiModelProperty(notes="广告名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="跳转链接：当MEDIA_TYPE=1时，为商城或者门店的商品跳转链接",allowEmptyValue=true,example="",allowableValues="")
	String link;

	
	@ApiModelProperty(notes="结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="是否启用=广告上线-1，广告下线-0",allowEmptyValue=true,example="",allowableValues="")
	String enabled;

	
	@ApiModelProperty(notes="开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="主要图片链接(大图)",allowEmptyValue=true,example="",allowableValues="")
	String imageUrl;

	
	@ApiModelProperty(notes="门店编号",allowEmptyValue=true,example="",allowableValues="")
	String locationId;

	
	@ApiModelProperty(notes="广告内容：当MEDIA_TYPE=1时，为商城或者门店的商品跳转链接的参数（商品编码）",allowEmptyValue=true,example="",allowableValues="")
	String content;

	
	@ApiModelProperty(notes="商户编号",allowEmptyValue=true,example="",allowableValues="")
	String shopId;

	
	@ApiModelProperty(notes="0-门店私有，1-商户私有，所有店铺共享，2-平台共有，所有商户共享",allowEmptyValue=true,example="",allowableValues="")
	String shareLvl;

	
	@ApiModelProperty(notes="投放类型CPM-按千次投放，CPC-按点击投放，CPS-按效果投放，CPT-按时间投放，",allowEmptyValue=true,example="",allowableValues="")
	String adType;

	
	@ApiModelProperty(notes="点击次数",allowEmptyValue=true,example="",allowableValues="")
	Integer clickNum;

	
	@ApiModelProperty(notes="展示次数",allowEmptyValue=true,example="",allowableValues="")
	Integer showNum;

	
	@ApiModelProperty(notes="真实投放结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date overTime;

	
	@ApiModelProperty(notes="购买单位数",allowEmptyValue=true,example="",allowableValues="")
	Integer saleNum;

	
	@ApiModelProperty(notes="排序",allowEmptyValue=true,example="",allowableValues="")
	Integer sortOrder;

	
	@ApiModelProperty(notes="展示概率0~100之间，默认100",allowEmptyValue=true,example="",allowableValues="")
	Integer showPct;

	
	@ApiModelProperty(notes="广告备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="小图，排行榜小图、推荐图标",allowEmptyValue=true,example="",allowableValues="")
	String extraImage;

	
	@ApiModelProperty(notes="商品价格：排行榜需要",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal goodsPrice;

	
	@ApiModelProperty(notes="小图标",allowEmptyValue=true,example="",allowableValues="")
	String smallImage;

	/**
	 *编号
	 **/
	public Advert(String id) {
		this.id = id;
	}
    
    /**
     * 广告
     **/
	public Advert() {
	}

}