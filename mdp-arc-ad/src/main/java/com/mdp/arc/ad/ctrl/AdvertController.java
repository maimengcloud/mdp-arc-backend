package com.mdp.arc.ad.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.ad.entity.Advert;
import com.mdp.arc.ad.service.AdvertService;
import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.qx.HasRole;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * url编制采用rest风格,如对MALL.pub_ad的操作有增删改查,对应的url分别为:<br>
 *  新增: pub/ad/add <br>
 *  查询: pub/ad/list<br>
 *  模糊查询: pub/ad/listKey<br>
 *  修改: pub/ad/edit <br>
 *  删除: pub/ad/del<br>
 *  批量删除: pub/ad/batchDel<br>
 * 组织 com.qqkj  顶级模块 mallm 大模块 pub 小模块 <br>
 * 实体 Ad 表 pub_ad 当前主键(包括多主键): id;
 ***/
@RestController("mdp.arc.ad.advertController")
@RequestMapping(value="/*/arc/ad/advert")
public class AdvertController {
	
	static Log logger=LogFactory.getLog(AdvertController.class);
	
	@Autowired
	private AdvertService adService;
	
	
		 
	
	/**
	 * 请求,如list
	 * 分页参数 {pageNum:1,pageSize:10,total:0}
	 * 根据条件查询数据对象列表,如果不上传分页参数，将不会分页。后台自动分页，无需编程
	 */
	@RequestMapping(value="/list")
	public Result listAd( @RequestParam Map<String,Object> params ){
		 
		IPage page= QueryTools.initPage(params);
		List<Map<String,Object>>	datas = adService.selectListMapByWhere(page,QueryTools.initQueryWrapper(Advert.class,params),params);	//列出Ad列表
		return Result.ok().setData(datas).setTotal(page.getTotal());
	}
	

	/**
	 * 请求模糊查询、条件字段由上传的参数决定 ,如{字段1:v1,字段2:v1},代表字段1，字段2都以v1作为参数值进行模糊查询，两个条件是or的关系。需要驼峰命名字段名
	 * 根据条件查询数据对象列表
	 
	@RequestMapping(value="/listKey")
	public Result listAdKey( @RequestParam Map<String,Object> params ){
		 
		List<Map<String,Object>>	adList = adService.selectListMapByKey(ad);	//列出Ad列表
		return Result.ok().setData(adList);
		
		return Result.ok();
	}
	*/
	
	/**
	 * 新增一条数据
	 */
	@RequestMapping(value="/add")
	@HasRole(roles= {"platformAdmin","shopAdmin"})
	public Result addAd(@RequestBody Advert ad) {
		
		
		try{
			adService.insert(ad);
			return Result.ok().setData(ad);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
	}
	
	
	/**
	 * 根据主键删除1条数据
	*/
	@RequestMapping(value="/del")
	@HasRole(roles= {"platformAdmin","shopAdmin"})
	public Result delAd(@RequestBody Advert ad){
		
		
		try{
			adService.deleteByPk(ad);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}  
		return Result.ok();
	}
	 
	
	/**
	 * 根据主键修改一条数据
	 */
	@RequestMapping(value="/edit")
	@HasRole(roles= {"platformAdmin","shopAdmin"})
	public Result editAd(@RequestBody Advert ad) {
		
		
		try{
			adService.updateByPk(ad);
			return Result.ok().setData(ad);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
	}
	
	

	
	/**
	 * 批量删除
	 */
	@HasRole(roles= {"platformAdmin","shopAdmin"})
	@RequestMapping(value="/batchDel")
	public Result batchDelAd(@RequestBody String[] ids) {
		
		
		List<Advert> list=new ArrayList<Advert>();
		try{
			for(int i=0;i<ids.length;i++){
				Advert ad=new Advert();
				ad.setId(ids[i]);
				list.add(ad);
			  }
			adService.batchDelete(list);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}  
		return Result.ok();
	}

	/**
	 * 根据主键update一条数据
	 */
	@RequestMapping(value="/updateAdInfo")
	@HasRole(roles= {"platformAdmin","shopAdmin"})
	public Result updateAd(@RequestBody Advert ad) {
		
		
		try{
			adService.updateSomeFieldByPk(ad);
			return Result.ok().setData(ad);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
	}

}
