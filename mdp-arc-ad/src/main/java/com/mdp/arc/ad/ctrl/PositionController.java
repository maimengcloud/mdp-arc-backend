package com.mdp.arc.ad.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.ad.entity.Position;
import com.mdp.arc.ad.service.PositionService;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.qx.HasRole;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * url编制采用rest风格,如对MALL.pub_ad_position的操作有增删改查,对应的url分别为:<br>
 * 新增: pub/adPosition/add <br>
 * 查询: pub/adPosition/list<br>
 * 模糊查询: pub/adPosition/listKey<br>
 * 修改: pub/adPosition/edit <br>
 * 删除: pub/adPosition/del<br>
 * 批量删除: pub/adPosition/batchDel<br>
 * 组织 com.qqkj 顶级模块 mallm 大模块 pub 小模块 <br>
 * 实体 AdPosition 表 pub_ad_position 当前主键(包括多主键): id;
 ***/
@RestController("mdp.arc.ad.positionController")
@RequestMapping(value="/*/arc/ad/position")
public class PositionController {

	static Log logger = LogFactory.getLog(PositionController.class);

	@Autowired
	private PositionService adPositionService;

	

	/**
	 * 请求,如list 分页参数 {pageNum:1,pageSize:10,total:0}
	 * 根据条件查询数据对象列表,如果不上传分页参数，将不会分页。后台自动分页，无需编程
	 */
	@RequestMapping(value = "/list")
	public Map<String, Object> listAdPosition(@RequestParam Map<String, Object> params) {
		Map<String, Object> m = new HashMap<>();
		IPage page= QueryTools.initPage(params);
		List<Map<String, Object>> datas = adPositionService.selectListMapByWhere(page,QueryTools.initQueryWrapper(Position.class,params),params); // 列出AdPosition列表
		return Result.ok().setData( datas);

	}

	/**
	 * 请求模糊查询、条件字段由上传的参数决定
	 * ,如{字段1:v1,字段2:v1},代表字段1，字段2都以v1作为参数值进行模糊查询，两个条件是or的关系。需要驼峰命名字段名 根据条件查询数据对象列表
	 * 
	 * @RequestMapping(value="/listKey") public Map<String,Object>
	 *                                   listAdPositionKey( @RequestParam
	 *                                   Map<String,Object> adPosition){
	 *                                   
	 *                                   List<Map<String,Object>> adPositionList =
	 *                                   adPositionService.selectListMapByKey(adPosition);
	 *                                   //列出AdPosition列表
	 *                                   return Result.ok().setData(adPositionList); Tips tips=new
	 *                                   Tips("查询成功"); m.put("tips", tips);
	 * 
	 *                                   return m; }
	 */

	/**
	 * 新增一条数据
	 */
	@RequestMapping(value = "/add")
	@HasRole(roles = { "platformAdmin", "shopAdmin" })
	public Map<String, Object> addAdPosition(@RequestBody Position adPosition) {
		Map<String, Object> m = new HashMap<>();
		Tips tips = new Tips("成功新增一条数据");
		try {
			adPositionService.insert(adPosition);
			return Result.ok().setData( adPosition);
		} catch (BizException e) {
			logger.error("执行异常", e);
			return Result.error(e);
		} catch (Exception e) {
			logger.error("执行异常", e);
			return Result.error(e.getMessage());
		}
	}

	/**
	 * 新增一条数据
	 * @cdate 2019/6/19 20:30
	 * @author lyk
	 */
	@RequestMapping(value = "/addonly")
	@HasRole(roles = { "platformAdmin", "shopAdmin" })
	public Map<String, Object> addAdPositionOnly(@RequestBody Position adPosition) {
		Map<String, Object> m = new HashMap<>();
		Tips tips = new Tips("成功新增一条数据");
		// 查询看有没有,一个商城只能存在1个广告
		// 添加
		try {
			adPositionService.addAdPositionOnly(adPosition);
			return Result.ok().setData( adPosition);
		} catch (BizException e) {
			logger.error("执行异常", e);
			return Result.error(e);
		} catch (Exception e) {
			logger.error("执行异常", e);
			return Result.error(e.getMessage());
		}
	}

	/**
	 * 根据主键删除1条数据
	 */
	@RequestMapping(value = "/del")
	@HasRole(roles = { "platformAdmin", "shopAdmin" })
	public Map<String, Object> delAdPosition(@RequestBody Position adPosition) {
		Map<String, Object> m = new HashMap<>();
		Tips tips = new Tips("成功删除一条数据");
		try {
			adPositionService.deleteByPk(adPosition);
		} catch (BizException e) {
			logger.error("执行异常", e);
			return Result.error(e);
		} catch (Exception e) {
			logger.error("执行异常", e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}

	/**
	 * 根据主键修改一条数据
	 */
	@RequestMapping(value = "/edit")
	@HasRole(roles = { "platformAdmin", "shopAdmin" })
	public Map<String, Object> editAdPosition(@RequestBody Position adPosition) {
		Map<String, Object> m = new HashMap<>();
		Tips tips = new Tips("成功更新一条数据");
		try {
			adPositionService.updateByPk(adPosition);
			return Result.ok().setData( adPosition);
		} catch (BizException e) {
			logger.error("执行异常", e);
			return Result.error(e);
		} catch (Exception e) {
			logger.error("执行异常", e);
			return Result.error(e.getMessage());
		}
	}

	/**
	 * 批量删除
	 */
	@RequestMapping(value = "/batchDel")
	@HasRole(roles = { "platformAdmin", "shopAdmin" })
	public Map<String, Object> batchDelAdPosition(@RequestBody String[] ids) {
		Map<String, Object> m = new HashMap<>();
		Tips tips = new Tips("成功删除" + ids.length + "条数据");
		List<Position> list = new ArrayList<Position>();
		try {
			for (int i = 0; i < ids.length; i++) {
				Position adPosition = new Position();
				adPosition.setId(ids[i]);
				list.add(adPosition);
			}
			adPositionService.batchDelete(list);
			return Result.ok();
		} catch (BizException e) {
			logger.error("执行异常", e);
			return Result.error(e);
		} catch (Exception e) {
			logger.error("执行异常", e);
			return Result.error(e.getMessage());
		}
	}

}
