package  com.mdp.arc.ad.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 ad  小模块 Position<br>
 */
@Data
@TableName("arc_ad_position")
@ApiModel(description="广告位置")
public class Position  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="位置名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="宽度",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal width;

	
	@ApiModelProperty(notes="高度",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal height;

	
	@ApiModelProperty(notes="位置描述",allowEmptyValue=true,example="",allowableValues="")
	String descs;

	
	@ApiModelProperty(notes="门店编号-暂时作废",allowEmptyValue=true,example="",allowableValues="")
	String locationId;

	
	@ApiModelProperty(notes="商户编号-暂时作废",allowEmptyValue=true,example="",allowableValues="")
	String shopId;

	
	@ApiModelProperty(notes="位置类型0商城1具体门店",allowEmptyValue=true,example="",allowableValues="")
	String posType;

	
	@ApiModelProperty(notes="位置代码(前台使用mall_banner商城首页轮播图mall_location_banner商城门店首页乱播图location_inner_banner门店内部轮播图)",allowEmptyValue=true,example="",allowableValues="")
	String posCode;

	
	@ApiModelProperty(notes="0-门店私有，1-商户私有，所有店铺共享，2-平台共有，所有商户共享",allowEmptyValue=true,example="",allowableValues="")
	String shareLvl;

	/**
	 *编号
	 **/
	public Position(String id) {
		this.id = id;
	}
    
    /**
     * 广告位置
     **/
	public Position() {
	}

}