package  com.mdp.arc.ad.service;

import com.mdp.arc.ad.entity.Advert;
import com.mdp.core.utils.BaseUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.Map;
/**
 * AdvertService的测试案例
 * 组织 com.mdp<br>
 * 顶级模块 arc<br>
 * 大模块 ad<br>
 * 小模块 <br>
 * 表 arc_ad_advert 广告<br>
 * 实体 Advert<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	id,positionId,mediaType,name,link,endTime,enabled,startTime,imageUrl,locationId,content,shopId,shareLvl,adType,clickNum,showNum,overTime,saleNum,sortOrder,showPct,remark,extraImage,goodsPrice,smallImage;<br>
 * 当前表的所有字段名:<br>
 *	id,position_id,media_type,name,link,end_time,enabled,start_time,image_url,location_id,content,shop_id,share_lvl,ad_type,click_num,show_num,over_time,sale_num,sort_order,show_pct,remark,extra_image,goods_price,small_image;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestAdvertService  {

	@Autowired
	AdvertService advertService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("id","ScXR","positionId","pVza","mediaType","37w5","name","sw1Z","link","YaDe","endTime",new Date("2023-08-28 13:54:55"),"enabled","E","startTime",new Date("2023-08-28 13:54:55"),"imageUrl","Nm1z","locationId","4D86","content","MsEY","shopId","Mf57","shareLvl","5","adType","3657","clickNum",1236,"showNum",5273,"overTime",new Date("2023-08-28 13:54:55"),"saleNum",8042,"sortOrder",4479,"showPct",7888,"remark","IkDT","extraImage","3O4u","goodsPrice",601.01,"smallImage","9ISC");
		Advert advert=BaseUtils.fromMap(p,Advert.class);
		advertService.save(advert);
		//Assert.assertEquals(1, result);
	}
	 
}
