package  com.mdp.arc.ad.service;

import com.mdp.arc.ad.entity.Position;
import com.mdp.core.utils.BaseUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;
/**
 * PositionService的测试案例
 * 组织 com.mdp<br>
 * 顶级模块 arc<br>
 * 大模块 ad<br>
 * 小模块 <br>
 * 表 arc_ad_position 广告位置<br>
 * 实体 Position<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	id,name,width,height,descs,locationId,shopId,posType,posCode,shareLvl;<br>
 * 当前表的所有字段名:<br>
 *	id,name,width,height,descs,location_id,shop_id,pos_type,pos_code,share_lvl;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestPositionService  {

	@Autowired
	PositionService positionService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("id","6ECr","name","13m8","width",4675,"height",8069,"descs","6COU","locationId","MO8h","shopId","4iB6","posType","C","posCode","ZbzD","shareLvl","x");
		Position position=BaseUtils.fromMap(p,Position.class);
		positionService.save(position);
		//Assert.assertEquals(1, result);
	}
	 
}
