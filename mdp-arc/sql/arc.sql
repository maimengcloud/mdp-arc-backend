/*
 Navicat Premium Data Transfer

 Source Server         : 123.207.117.5
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : 123.207.117.5:3306
 Source Schema         : arc

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 28/07/2024 23:45:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for arc_ad_advert
-- ----------------------------
DROP TABLE IF EXISTS `arc_ad_advert`;
CREATE TABLE `arc_ad_advert`  (
  `ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '编号',
  `POSITION_ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL DEFAULT '0' COMMENT '广告位置',
  `MEDIA_TYPE` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '媒体类型:0 图片、文件，1商品',
  `NAME` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '广告名称',
  `LINK` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '跳转链接：当MEDIA_TYPE=1时，为商城或者门店的商品跳转链接',
  `END_TIME` datetime NOT NULL COMMENT '结束时间',
  `ENABLED` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL DEFAULT '1' COMMENT '是否启用=广告上线-1，广告下线-0',
  `START_TIME` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `IMAGE_URL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主要图片链接(大图)',
  `LOCATION_ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '门店编号',
  `CONTENT` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '广告内容：当MEDIA_TYPE=1时，为商城或者门店的商品跳转链接的参数（商品编码）',
  `SHOP_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '商户编号',
  `SHARE_LVL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '0-门店私有，1-商户私有，所有店铺共享，2-平台共有，所有商户共享',
  `AD_TYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '投放类型CPM-按千次投放，CPC-按点击投放，CPS-按效果投放，CPT-按时间投放，',
  `CLICK_NUM` int NULL DEFAULT NULL COMMENT '点击次数',
  `SHOW_NUM` int NULL DEFAULT NULL COMMENT '展示次数',
  `OVER_TIME` datetime NULL DEFAULT NULL COMMENT '真实投放结束时间',
  `SALE_NUM` int NULL DEFAULT NULL COMMENT '购买单位数',
  `SORT_ORDER` int NULL DEFAULT NULL COMMENT '排序',
  `SHOW_PCT` int NULL DEFAULT NULL COMMENT '展示概率0~100之间，默认100',
  `REMARK` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '广告备注',
  `extra_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '小图，排行榜小图、推荐图标',
  `goods_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '商品价格：排行榜需要',
  `small_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '小图标',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `AD_IDX_AD_POSITION_ID`(`POSITION_ID` ASC) USING BTREE,
  INDEX `AD_IDX_ENABLED`(`ENABLED` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '广告' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_ad_position
-- ----------------------------
DROP TABLE IF EXISTS `arc_ad_position`;
CREATE TABLE `arc_ad_position`  (
  `ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '编号',
  `NAME` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '位置名称',
  `WIDTH` decimal(5, 0) NULL DEFAULT 0 COMMENT '宽度',
  `HEIGHT` decimal(5, 0) NULL DEFAULT 0 COMMENT '高度',
  `DESCS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '位置描述',
  `LOCATION_ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '门店编号-暂时作废',
  `SHOP_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '商户编号-暂时作废',
  `pos_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '位置类型0商城1具体门店',
  `pos_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '位置代码(前台使用mall_banner商城首页轮播图mall_location_banner商城门店首页乱播图location_inner_banner门店内部轮播图)',
  `SHARE_LVL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '0-门店私有，1-商户私有，所有店铺共享，2-平台共有，所有商户共享',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '广告位置' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_aq_category-作废
-- ----------------------------
DROP TABLE IF EXISTS `arc_aq_category-作废`;
CREATE TABLE `arc_aq_category-作废`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '分类编号',
  `CATEGORY_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '分类名称',
  `IS_SHOW` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否显示',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '常见问题分类作废-改为字典 aq_category' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_aq_comment
-- ----------------------------
DROP TABLE IF EXISTS `arc_aq_comment`;
CREATE TABLE `arc_aq_comment`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '评论人',
  `USERNAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '评论人姓名',
  `STAR` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '星级',
  `CREATE_DATE` datetime NULL DEFAULT NULL COMMENT '时间',
  `QUESTION_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '问题编号',
  `PCOMMENT_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '上级评论 编号',
  `PRAISE_SUM` decimal(10, 0) NULL DEFAULT NULL COMMENT '点赞数量',
  `IS_SHOW` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否显示0否1是',
  `TO_USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '接收本条评论的用户编号',
  `TO_USERNAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '接收本条评论的用户名称',
  `LVL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '层级0,1,2,3,4',
  `DEPTID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '部门',
  `CONTEXT` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '评论内容',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `ARC_AQ_COMMENT_USER`(`USERID` ASC, `BRANCH_ID` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '问题评论表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_aq_faq
-- ----------------------------
DROP TABLE IF EXISTS `arc_aq_faq`;
CREATE TABLE `arc_aq_faq`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `TITLE` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标题',
  `CATEGORY_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '分类',
  `REPLY_ARCHIVE_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '回复文章',
  `REPLY_URL` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '回复链接',
  `IS_SHOW` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否显示',
  `REPLY_CONTEXT` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '回复内容',
  `ASK_CONTEXT` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '问询内容',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '常见问题表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_aq_question
-- ----------------------------
DROP TABLE IF EXISTS `arc_aq_question`;
CREATE TABLE `arc_aq_question`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `TITLE` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标题',
  `CATEGORY_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '分类',
  `ASK_USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '咨询人员编号',
  `ASK_PHONE` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '咨询人员手机号',
  `ASK_USERNAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '咨询人员名称',
  `STATUS` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '状态0询问中1已回复2已结束',
  `IS_FAQ` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否常见问题',
  `ASK_DEPTID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '询问部门',
  `ASK_DATE` datetime NULL DEFAULT NULL COMMENT '咨询时间',
  `ASK_CONTEXT` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '问题内容',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `ARC_AQ_QUESTION_USER`(`BRANCH_ID` ASC, `ASK_USERID` ASC, `ASK_USERNAME` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '问题表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_archive
-- ----------------------------
DROP TABLE IF EXISTS `arc_archive`;
CREATE TABLE `arc_archive`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `TAG_NAMES` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标签名多个,分割',
  `DEPTID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '创建部门',
  `USERID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '创建人',
  `IS_SHARE` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '是否共享',
  `URL` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '访问路径',
  `ARCHIVE_ABSTRACT` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '摘要',
  `ARCHIVE_CONTEXT` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '内容',
  `ARCHIVE_TITLE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标题',
  `ARCHIVING_USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '归档人',
  `IS_FROM_ARCHIVING` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否来自归档',
  `ARCHIVING_DATE` datetime NULL DEFAULT NULL COMMENT '归档日期',
  `CREATE_DATE` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `AUTHOR_USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '作者编号',
  `AUTHOR_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '作者名称',
  `CAN_DEL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '是否可删除',
  `CAN_EDIT` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '是否可修改',
  `CAN_READ` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '1' COMMENT '是否可读',
  `BIZ_KEY` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '业务编号',
  `ENCRYPT_TYPE` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '加密方式0不加密1私钥2公钥',
  `CAN_COMMENT` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '开放评论',
  `STATUS` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '0草稿1已上架2已下架3审核中',
  `IS_STORAGE_DIR` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否存到硬盘',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  `TITLE_IMG_URL` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标题图片链接',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT 'movie:视频(ARCHIVE_CONTEXT,存视频地址)',
  `duration` int NULL DEFAULT NULL COMMENT '时长:如果是视频,记录视频时长',
  `TAG_IDS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标签编号列表,逗号分隔',
  `archive_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '0|知识库\r\n1|新闻\r\n2|内部公告\r\n3|平台公告\r\n5|论坛\r\n6|公文\r\n4|其它\r\n7|归档\r\n8|网站栏目,管理元数据categoryType',
  `image_urls` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '图文说明\r\n[{imgUrl:\'\',imgDesc:\'\',linkUrl:\'跳转链接\',ext:\'扩展字段\'}]',
  `link_archives` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '关联文章列表[{id:\'\',imgUrl:\'\',imgDesc:\'\',linkUrl:\'跳转链接\',ext:\'扩展字段\'}]',
  `read_nums` int NULL DEFAULT NULL COMMENT '阅读人数',
  `comment_nums` int NULL DEFAULT NULL COMMENT '评论人数',
  `link_ads` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '关联广告\r\n[{id:\'\',imgUrl:\'\',imgDesc:\'\',linkUrl:\'跳转链接\',ext:\'扩展字段\'}]',
  `link_users` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '关联用户\r\n[{imgUrl:\'\',imgDesc:\'\',linkUrl:\'跳转链接\',ext:\'扩展字段\'}]',
  `ext_infos` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '扩展字段\r\n[{name:\'中文名称\',id:\'编号\',value:\'值\',remark:\'备注\',type:\'支持简单的1-普通文本2-数字，3-日期，8-富文本，9单图文，15-是否\'}]',
  `PRAISE_SUM` decimal(10, 0) NULL DEFAULT NULL COMMENT '点赞的次数',
  `is_hot` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '火热0否1是',
  `is_main` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '推荐0否1是',
  `op_date` datetime NULL DEFAULT NULL COMMENT '最后更新日期',
  `seq_no` int NULL DEFAULT 999 COMMENT '排序',
  `PBIZ_KEY` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '上级业务编号',
  `ctime` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `IP` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT 'ip地址',
  `city_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '城市编号',
  `city_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '城市名称',
  `up_nums` int NULL DEFAULT NULL COMMENT '点赞数',
  `collect_nums` int NULL DEFAULT NULL COMMENT '收藏数',
  `rely_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考类型，开放式字段，1-开源社区，2-项目论坛,逗号 分割多个',
  `rely_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考编号,逗号 分割多个',
  `rely_stype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考子类型,逗号 分割多个',
  `rely_sids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考子编号,逗号 分割多个',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `ARC_ARCHIVE_I_BRANCH`(`BRANCH_ID` ASC, `DEPTID` ASC) USING BTREE,
  INDEX `ARC_ARCHIVE_I_USER`(`BRANCH_ID` ASC, `USERID` ASC, `AUTHOR_NAME` ASC) USING BTREE,
  INDEX `CREATE_DATE`(`CREATE_DATE` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '档案信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_archive_category
-- ----------------------------
DROP TABLE IF EXISTS `arc_archive_category`;
CREATE TABLE `arc_archive_category`  (
  `ARCHIVE_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '文章编号',
  `CATEGORY_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '分类编号',
  PRIMARY KEY (`ARCHIVE_ID`, `CATEGORY_ID`) USING BTREE,
  CONSTRAINT `arc_archive_category_ibfk_1` FOREIGN KEY (`ARCHIVE_ID`) REFERENCES `arc_archive` (`ID`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '文章分类关系表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_archive_collect
-- ----------------------------
DROP TABLE IF EXISTS `arc_archive_collect`;
CREATE TABLE `arc_archive_collect`  (
  `ARCHIVE_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '文章编号',
  `USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '收藏人员编号',
  `ctime` datetime NULL DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`ARCHIVE_ID`, `USERID`) USING BTREE,
  CONSTRAINT `arc_archive_collect_ibfk_1` FOREIGN KEY (`ARCHIVE_ID`) REFERENCES `arc_archive` (`ID`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '文章收藏人员列表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_archive_comment
-- ----------------------------
DROP TABLE IF EXISTS `arc_archive_comment`;
CREATE TABLE `arc_archive_comment`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '评论人',
  `USERNAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '评论人姓名',
  `STAR` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '星级',
  `CREATE_DATE` datetime NULL DEFAULT NULL COMMENT '时间',
  `ARCHIVE_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '档案编号',
  `PCOMMENT_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '上级评论 编号',
  `PRAISE_SUM` decimal(10, 0) NULL DEFAULT 0 COMMENT '点赞数量',
  `IS_SHOW` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否显示0否1是',
  `TO_USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '回复用户编号',
  `TO_USERNAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '回复用户名',
  `LVL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '层级0,1,2,3,4',
  `CONTEXT` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '评论内容',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  `IP` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT 'ip地址',
  `city_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '城市编号',
  `city_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '城市名称',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '状态0未审核，1已审核，3审核不通过',
  `child_nums` int NULL DEFAULT NULL COMMENT '儿子节点数量',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '档案评论表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_archive_up
-- ----------------------------
DROP TABLE IF EXISTS `arc_archive_up`;
CREATE TABLE `arc_archive_up`  (
  `ARCHIVE_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '文章编号',
  `USERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '点赞人员编号',
  `utime` datetime NULL DEFAULT NULL COMMENT '点赞时间',
  PRIMARY KEY (`ARCHIVE_ID`, `USERID`) USING BTREE,
  CONSTRAINT `arc_archive_up_ibfk_1` FOREIGN KEY (`ARCHIVE_ID`) REFERENCES `arc_archive` (`ID`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '文章点赞人员列表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_attachment
-- ----------------------------
DROP TABLE IF EXISTS `arc_attachment`;
CREATE TABLE `arc_attachment`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '附件名称',
  `URL` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '访问路径',
  `RELATIVE_PATH` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '硬盘存放路径',
  `FILE_SUFFIX` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '后缀名',
  `CDN_URL` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '内容加速器访问路径',
  `IS_IMG` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否图片',
  `ARCHIVE_ID` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '档案主编号',
  `IS_CDN` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否使用CDN',
  `ROOT_PATH` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '根目录',
  `CREATE_DATE` datetime NULL DEFAULT NULL COMMENT '存入时间',
  `CAN_DOWN` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否可下载',
  `CAN_DEL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否可删除',
  `CAN_READ` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否可读',
  `BIZ_ID` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '业务编号、产品编号、商品编号等',
  `REMARK` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '业务名称、产品名称、商品名称等',
  `STORE_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '存储名字',
  `FILE_SIZE` decimal(8, 0) NULL DEFAULT NULL COMMENT '文件大小',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '云用户机构编号',
  `DEPTID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '部门编号',
  `archive_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '0|知识库\r\n1|新闻\r\n2|内部公告\r\n3|平台公告\r\n5|论坛\r\n6|公文\r\n4|其它\r\n7|归档\r\n8|网站栏目,管理元数据categoryType',
  `CATEGORY_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '分类编号',
  `rely_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考类型',
  `rely_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考编号',
  `rely_stype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考子类型',
  `rely_sid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考子编号',
  `cuserid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '创建人编号',
  `cusername` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '创建人姓名',
  `cdate` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `ext_infos` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '扩展字段',
  `tag_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标签',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '档案附件表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_category
-- ----------------------------
DROP TABLE IF EXISTS `arc_category`;
CREATE TABLE `arc_category`  (
  `category_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '0知识库-1新闻类2企业内部通知公告类3平台通知公告4其它5论坛6公文7归档8网站栏目,关联基础数据categoryType',
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '分类主键',
  `PID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '上级分类',
  `NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '分类名称',
  `SORT_ORDER` decimal(5, 0) NULL DEFAULT NULL COMMENT '分类排序',
  `IS_SHOW` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否显示',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  `image_urls` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '图片列表逗号分割',
  `is_leaf` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否叶子节点0否1是，叶子节点不允许再建立下级，非叶子节点不允许挂文章',
  `limit_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '文章限制,1-单篇文章，2-多篇文章',
  `is_auth` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '文章是否需要审核0-否1是',
  `paths` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '上级分类路径，逗号分割，包括自身',
  `crely_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考类型',
  `crely_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考类型编号',
  `crely_stype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '子参考类型',
  `crely_sid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '子参考类型编号',
  `qx_lvl` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '权限控制要求 0-完全开放，1-内部完全开放，2-受限',
  `pqx` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否强制要求子类继承',
  `ext_infos` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '扩展字段',
  `cuserid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '创建人编号',
  `tag_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标签',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `ARC_C_BRANCH_INDEX`(`BRANCH_ID` ASC) USING BTREE,
  INDEX `ARC_C_PI_INDEX`(`PID` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '档案类目' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_category_qx
-- ----------------------------
DROP TABLE IF EXISTS `arc_category_qx`;
CREATE TABLE `arc_category_qx`  (
  `CATE_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '分类编号',
  `QRY_ROLEIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许那些角色查询,号分割',
  `QRY_DEPTIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许那些部门查询,号分割',
  `QRY_USERIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许哪些人查询,号分割',
  `NQ_ROLEIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些角色查询',
  `NQ_DEPTIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些部门查询',
  `NQ_USERIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些人查询',
  `OTH_QUERY` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否允许其它人查询',
  `OTH_EDIT` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否允许其它人修改',
  `OTH_DEL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否允许其它人删除',
  `LVL_CHECK` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否进行部门级别传递权限检查',
  `QRY_MIN_LVL` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '最低级别查询权限',
  `EDIT_ROLEIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许那些角色更新,号分割',
  `EDIT_DEPTIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许那些部门更新,号分割',
  `EDIT_USERIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许哪些人更新号分割',
  `NE_ROLEIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些角色更新',
  `NE_DEPTIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些部门更新',
  `NE_USERIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些人更新',
  `DEL_ROLEIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许那些角色删除,号分割',
  `DEL_DEPTIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许那些部门删除,号分割',
  `DEL_USERIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '允许哪些人删除,号分割',
  `ND_ROLEIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些角色删除',
  `ND_DEPTIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些部门删除',
  `ND_USERIDS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '禁止哪些人查询',
  `EDIT_MIN_LVL` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '最低级别更新权限',
  `DEL_MIN_LVL` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '最低级别删除权限',
  PRIMARY KEY (`CATE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '表单权限' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_channel
-- ----------------------------
DROP TABLE IF EXISTS `arc_channel`;
CREATE TABLE `arc_channel`  (
  `ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '频道编号',
  `NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '频道名称',
  `URL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '频道链接',
  `ICON_URL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '图标链接',
  `SORT_ORDER` decimal(4, 0) NULL DEFAULT 10 COMMENT '排序',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  `CATEGORY_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '内容分类编号',
  `IS_TO_URL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '是否自定义跳转链接',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '内容频道信息' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_doc_archive
-- ----------------------------
DROP TABLE IF EXISTS `arc_doc_archive`;
CREATE TABLE `arc_doc_archive`  (
  `signer` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签发人',
  `archive_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '公文编号',
  `word_num` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '字号',
  `written_date` datetime NULL DEFAULT NULL COMMENT '成文日期',
  `dispatch_date` datetime NULL DEFAULT NULL COMMENT '分发日期',
  `draft_userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '拟稿人',
  `draft_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '拟稿人姓名',
  `check_userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '核稿人',
  `check_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '核稿人姓名',
  `checked_userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '校对人编号',
  `checked_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '校队人姓名',
  `secrecy_level` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '保密级别',
  `urgency_level` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '紧急程度',
  `topic_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '主题词',
  `topic_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '主题词Id',
  `doc_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标题',
  `doc_flow_state` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '流程状态',
  `doc_proc_inst_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '流程实例编号',
  `doc_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '文档编号=字名+（档案年度）+字号',
  `doc_ymd` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '文档年月日yyyyMMdd',
  `word_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '字名',
  `doc_year` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '档案年度',
  `doc_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '档案种类',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构号',
  `doc_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `sign_userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签报用户id',
  `sign_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签报用户名称',
  `sign_deptid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签报用户部门id',
  `sign_deptname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签报用户部门名称',
  `sign_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签报编号',
  `sign_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签报标题',
  `sign_nav` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签报内容',
  `office_options` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '办公厅意见',
  `leader_options` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '领导意见',
  `doc_category` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '文档种类，zffw:政府发文 gzqb:工作签报 xzsw:行政收文，sjdwlw:上级单位来文',
  `come_deptid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '来文机关id',
  `come_deptname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '来文机关名称',
  `receive_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '收文编号',
  `wen_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '文号',
  `niban_options` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '拟办意见',
  `leader_comments` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '领导批示',
  `cleader_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '批示的领导id',
  `cleader_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '批示的领导名称',
  `dleader_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '部门领导id',
  `dleader_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '部门领导名称',
  `receive_deptid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '收文的部门id',
  `receive_deptname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '收文的部门名称',
  `receive_date` datetime NULL DEFAULT NULL COMMENT '收文日期',
  PRIMARY KEY (`doc_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '文档库' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_doc_circulate
-- ----------------------------
DROP TABLE IF EXISTS `arc_doc_circulate`;
CREATE TABLE `arc_doc_circulate`  (
  `target_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '传阅对象类型dept部门/user用户',
  `target_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '传阅对象编号',
  `target_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '传阅对象名称',
  `doc_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '文档编号',
  `authority` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT 'read读/edit编辑/full完全控制',
  `can_download` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否可下载',
  `read_time` datetime NULL DEFAULT NULL COMMENT '传阅时间',
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `target_branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '归属机构',
  `doc_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '档案编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '传阅对象' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_doc_operlog
-- ----------------------------
DROP TABLE IF EXISTS `arc_doc_operlog`;
CREATE TABLE `arc_doc_operlog`  (
  `doc_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '文档编号',
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '日志编号',
  `oper_userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '操作者编号',
  `oper_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '操作者姓名',
  `oper_date` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `oper_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '操作类型read阅读/download下载',
  `client_ip` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '客户端ip地址',
  `doc_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '文档标题',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '备注',
  `op_branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '操作者归属机构号',
  `doc_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '文档操作日志' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_doc_send
-- ----------------------------
DROP TABLE IF EXISTS `arc_doc_send`;
CREATE TABLE `arc_doc_send`  (
  `send_date` datetime NULL DEFAULT NULL COMMENT '发送时间',
  `send_userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '发送人 ',
  `send_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '发送人姓名',
  `send_deptid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '发送部门',
  `send_dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '发送部门名称',
  `send_branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '发送机构号',
  `doc_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '文档编号',
  `send_flow_state` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '流程状态',
  `send_proc_inst_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '流程实例编号',
  `signed_userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签章动作人',
  `signed_deptid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签章单位',
  `signed_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签章动作人姓名',
  `signed_dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '签章单位名',
  `signed_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '印章编号',
  `doc_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  PRIMARY KEY (`doc_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '发文' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_doc_send_target
-- ----------------------------
DROP TABLE IF EXISTS `arc_doc_send_target`;
CREATE TABLE `arc_doc_send_target`  (
  `target_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '对象类型user用户/dept部门',
  `target_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '对象编号',
  `target_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '对象名称',
  `authority` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT 'read读/edit编辑/full完全控制',
  `can_download` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否可下载',
  `is_receive` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否已接收',
  `receive_date` datetime NULL DEFAULT NULL COMMENT '接收时间',
  `receive_deptid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '接收部门',
  `receive_userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '接收人编号',
  `receive_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '接收人姓名',
  `receive_dept_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '接收部门名称',
  `doc_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '文档编号',
  `send_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT 'main主送/carbon抄送',
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `target_branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '对象归属机构号',
  `doc_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '发文对象' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_doc_topic
-- ----------------------------
DROP TABLE IF EXISTS `arc_doc_topic`;
CREATE TABLE `arc_doc_topic`  (
  `topic_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主题编号',
  `topic_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '主题名称',
  `topic_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '备注说明',
  `branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构号',
  `is_pub` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否公共',
  PRIMARY KEY (`topic_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '文档主题-作废-改为字典-doc_topic' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_doc_type
-- ----------------------------
DROP TABLE IF EXISTS `arc_doc_type`;
CREATE TABLE `arc_doc_type`  (
  `type_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '类型编号',
  `type_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '类型名称',
  `type_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '类型备注',
  `branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '归属机构号',
  `is_pub` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否公共',
  PRIMARY KEY (`type_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '文档种类-作废-改为字典doc_type' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_doc_word_name
-- ----------------------------
DROP TABLE IF EXISTS `arc_doc_word_name`;
CREATE TABLE `arc_doc_word_name`  (
  `word_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '字名',
  `branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '归属机构号',
  `deptid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '归属部门编号',
  `word_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '字名编号',
  PRIMARY KEY (`word_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '文档种类-作废-改为字典doc_word_name' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_image
-- ----------------------------
DROP TABLE IF EXISTS `arc_image`;
CREATE TABLE `arc_image`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '附件名称',
  `URL` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '访问路径',
  `RELATIVE_PATH` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '硬盘存放路径',
  `FILE_SUFFIX` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '后缀名',
  `ROOT_PATH` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '根目录',
  `CREATE_DATE` datetime NULL DEFAULT NULL COMMENT '存入时间',
  `FILE_SIZE` decimal(8, 0) NULL DEFAULT NULL COMMENT '文件大小',
  `DEPTID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '归属部门',
  `TAG` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标签',
  `REMARK` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '备注信息',
  `CATEGORY_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '图片分类',
  `STORAGE_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '硬盘存储名字(不带后缀)',
  `URL_PREFIX` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '链接前缀',
  `IS_OUT_URL` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '0' COMMENT '是否外部链接',
  `OUT_URL` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '外部链接',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  `archive_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '0|知识库\r\n1|新闻\r\n2|内部公告\r\n3|平台公告\r\n5|论坛\r\n6|公文\r\n4|其它\r\n7|归档\r\n8|网站栏目,管理元数据categoryType',
  `rely_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考类型，开放式字段，1-开源社区，2-项目论坛,逗号 分割多个',
  `rely_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考编号,逗号 分割多个',
  `rely_sub_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考子类型,逗号 分割多个',
  `rely_sub_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考子编号,逗号 分割多个',
  `cuserid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '创建人编号',
  `cusername` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '创建人姓名',
  `cdate` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `ARC_IMAGE_INDEX_C`(`CATEGORY_ID` ASC, `BRANCH_ID` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '图片素材库' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_image_category
-- ----------------------------
DROP TABLE IF EXISTS `arc_image_category`;
CREATE TABLE `arc_image_category`  (
  `ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '主键',
  `CATEGORY_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '分类名称',
  `BRANCH_ID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '机构编号',
  `PID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '上一级',
  `is_pub` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否是公共分类的图片 1是0 否',
  `ext_infos` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '扩展字段',
  `crely_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考类型',
  `crely_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '参考类型编号',
  `crely_stype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '子参考类型',
  `crely_sid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '子参考类型编号',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci COMMENT = '图片分类' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_tag
-- ----------------------------
DROP TABLE IF EXISTS `arc_tag`;
CREATE TABLE `arc_tag`  (
  `tag_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标签名',
  `branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '机构号',
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '标签编号',
  `shop_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '商户编号',
  `category_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '标签分组',
  `is_pub` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否公共0否1是',
  PRIMARY KEY (`id`, `branch_id`) USING BTREE,
  INDEX `categoryIdIndex`(`category_id` ASC) USING BTREE,
  INDEX `branch_id`(`branch_id` ASC, `category_id` ASC, `is_pub` ASC) USING BTREE,
  INDEX `tag_name`(`branch_id` ASC, `category_id` ASC, `tag_name` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for arc_tag_category
-- ----------------------------
DROP TABLE IF EXISTS `arc_tag_category`;
CREATE TABLE `arc_tag_category`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '分组编号',
  `branch_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NOT NULL DEFAULT '' COMMENT '机构号',
  `shop_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '' COMMENT '商户编号',
  `category_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '' COMMENT '分组名称',
  `is_pub` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT NULL COMMENT '是否公共0否1是',
  `ext_infos` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '扩展字段',
  PRIMARY KEY (`id`, `branch_id`) USING BTREE,
  INDEX `branch_id`(`branch_id` ASC, `is_pub` ASC) USING BTREE,
  INDEX `shopidIndex`(`branch_id` ASC, `category_name` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_croatian_ci ROW_FORMAT = COMPACT;

SET FOREIGN_KEY_CHECKS = 1;
