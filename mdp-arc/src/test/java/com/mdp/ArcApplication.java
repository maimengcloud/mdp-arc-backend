package com.mdp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@SpringCloudApplication
//@EnableSwagger2
public class ArcApplication   {
	
	
	public static void main(String[] args) {
		SpringApplication.run(ArcApplication.class,args);

	}

}
