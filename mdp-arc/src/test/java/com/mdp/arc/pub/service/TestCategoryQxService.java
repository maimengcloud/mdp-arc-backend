package  com.mdp.arc.pub.service;

import com.mdp.arc.pub.entity.CategoryQx;
import com.mdp.core.utils.BaseUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;
/**
 * @author code-gen
 * @since 2023-9-15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestCategoryQxService  {

	@Autowired
	CategoryQxService categoryQxService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("cateId","0Kfy","qryRoleids","bd3G","qryDeptids","mCPX","qryUserids","S92Z","nqRoleids","O6e5","nqDeptids","Pa7h","nqUserids","87qR","othQuery","p","othEdit","i","othDel","g","lvlCheck","M","qryMinLvl","ZkNm","editRoleids","QNot","editDeptids","Nvax","editUserids","ZDx8","neRoleids","JMYa","neDeptids","7w97","neUserids","yd37","delRoleids","84QL","delDeptids","Wrw2","delUserids","NJEO","ndRoleids","H3GM","ndDeptids","5XfU","ndUserids","8lWJ","editMinLvl","e5L1","delMinLvl","v2uC");
		CategoryQx categoryQx=BaseUtils.fromMap(p,CategoryQx.class);
		categoryQxService.save(categoryQx);
		//Assert.assertEquals(1, result);
	}
	 
}
