package  com.mdp.arc.pub.service;

import com.mdp.arc.pub.entity.Category;
import com.mdp.core.utils.BaseUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;
/**
 * @author code-gen
 * @since 2023-9-15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestCategoryService  {

	@Autowired
	CategoryService categoryService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("categoryType","f","id","xjUT","pid","rb0T","name","D85v","sortOrder",3949,"isShow","V","branchId","i1Ru","imageUrls","cwni","isLeaf","7","limitType","u","isAuth","R","paths","RHid","crelyType","vtOl","crelyIds","1mKX","crelyStype","7HXj","crelySids","6FmI","qxLvl","u","pqx","t");
		Category category=BaseUtils.fromMap(p,Category.class);
		categoryService.save(category);
		//Assert.assertEquals(1, result);
	}
	 
}
