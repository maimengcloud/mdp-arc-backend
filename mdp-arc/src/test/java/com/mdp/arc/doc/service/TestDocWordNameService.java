package  com.mdp.arc.doc.service;

import com.mdp.arc.doc.entity.DocWordName;
import com.mdp.core.utils.BaseUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;
/**
 * DocWordNameService的测试案例
 * 组织 com.mdp<br>
 * 顶级模块 arc<br>
 * 大模块 doc<br>
 * 小模块 <br>
 * 表 arc_doc_word_name 文档种类-作废-改为字典doc_word_name<br>
 * 实体 DocWordName<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	wordName,branchId,deptid,wordId;<br>
 * 当前表的所有字段名:<br>
 *	word_name,branch_id,deptid,word_id;<br>
 * 当前主键(包括多主键):<br>
 *	word_id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestDocWordNameService  {

	@Autowired
	DocWordNameService docWordNameService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("wordName","6dN0","branchId","IzkX","deptid","1eyD","wordId","XMsE");
		DocWordName docWordName=BaseUtils.fromMap(p,DocWordName.class);
		docWordNameService.save(docWordName);
		//Assert.assertEquals(1, result);
	}
	 
}
