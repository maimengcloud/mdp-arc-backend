package  com.mdp.arc.doc.service;

import com.mdp.arc.doc.entity.DocType;
import com.mdp.core.utils.BaseUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;
/**
 * DocTypeService的测试案例
 * 组织 com.mdp<br>
 * 顶级模块 arc<br>
 * 大模块 doc<br>
 * 小模块 <br>
 * 表 arc_doc_type 文档种类-作废-改为字典doc_type<br>
 * 实体 DocType<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	typeId,typeName,typeRemark,branchId,isPub;<br>
 * 当前表的所有字段名:<br>
 *	type_id,type_name,type_remark,branch_id,is_pub;<br>
 * 当前主键(包括多主键):<br>
 *	type_id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestDocTypeService  {

	@Autowired
	DocTypeService docTypeService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("typeId","T12k","typeName","7H8D","typeRemark","jNw1","branchId","2o1q","isPub","x");
		DocType docType=BaseUtils.fromMap(p,DocType.class);
		docTypeService.save(docType);
		//Assert.assertEquals(1, result);
	}
	 
}
