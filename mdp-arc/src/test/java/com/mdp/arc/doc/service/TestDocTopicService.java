package  com.mdp.arc.doc.service;

import com.mdp.arc.doc.entity.DocTopic;
import com.mdp.core.utils.BaseUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;
/**
 * DocTopicService的测试案例
 * 组织 com.mdp<br>
 * 顶级模块 arc<br>
 * 大模块 doc<br>
 * 小模块 <br>
 * 表 arc_doc_topic 文档主题-作废-改为字典-doc_topic<br>
 * 实体 DocTopic<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	topicId,topicName,topicRemark,branchId,isPub;<br>
 * 当前表的所有字段名:<br>
 *	topic_id,topic_name,topic_remark,branch_id,is_pub;<br>
 * 当前主键(包括多主键):<br>
 *	topic_id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestDocTopicService  {

	@Autowired
	DocTopicService docTopicService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("topicId","IQ9U","topicName","8STf","topicRemark","QzN3","branchId","wZCM","isPub","w");
		DocTopic docTopic=BaseUtils.fromMap(p,DocTopic.class);
		docTopicService.save(docTopic);
		//Assert.assertEquals(1, result);
	}
	 
}
