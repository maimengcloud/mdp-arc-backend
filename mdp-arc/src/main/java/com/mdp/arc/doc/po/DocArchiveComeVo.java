package com.mdp.arc.doc.po;

import com.mdp.arc.doc.entity.DocArchive;
import com.mdp.arc.doc.entity.DocCirculate;

import java.util.List;

/**
 * 收文登记vo类
 */
public class DocArchiveComeVo {

    private DocArchive docArchive;

    private List<DocCirculate> docCirculates;

    public DocArchive getDocArchive() {
        return docArchive;
    }

    public void setDocArchive(DocArchive docArchive) {
        this.docArchive = docArchive;
    }

    public List<DocCirculate> getDocCirculates() {
        return docCirculates;
    }

    public void setDocCirculates(List<DocCirculate> docCirculates) {
        this.docCirculates = docCirculates;
    }

    @Override
    public String toString() {
        return "DocArchiveComeVo{" +
                "docArchive=" + docArchive +
                ", docCirculates=" + docCirculates +
                '}';
    }
}
