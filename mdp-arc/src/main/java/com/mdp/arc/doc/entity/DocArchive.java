package  com.mdp.arc.doc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 doc  小模块 DocArchive<br>
 */
@Data
@TableName("arc_doc_archive")
@ApiModel(description="文档库")
public class DocArchive  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String docId;

	
	@ApiModelProperty(notes="签发人",allowEmptyValue=true,example="",allowableValues="")
	String signer;

	
	@ApiModelProperty(notes="公文编号",allowEmptyValue=true,example="",allowableValues="")
	String archiveId;

	
	@ApiModelProperty(notes="字号",allowEmptyValue=true,example="",allowableValues="")
	String wordNum;

	
	@ApiModelProperty(notes="成文日期",allowEmptyValue=true,example="",allowableValues="")
	Date writtenDate;

	
	@ApiModelProperty(notes="分发日期",allowEmptyValue=true,example="",allowableValues="")
	Date dispatchDate;

	
	@ApiModelProperty(notes="拟稿人",allowEmptyValue=true,example="",allowableValues="")
	String draftUserid;

	
	@ApiModelProperty(notes="拟稿人姓名",allowEmptyValue=true,example="",allowableValues="")
	String draftUsername;

	
	@ApiModelProperty(notes="核稿人",allowEmptyValue=true,example="",allowableValues="")
	String checkUserid;

	
	@ApiModelProperty(notes="核稿人姓名",allowEmptyValue=true,example="",allowableValues="")
	String checkUsername;

	
	@ApiModelProperty(notes="校对人编号",allowEmptyValue=true,example="",allowableValues="")
	String checkedUserid;

	
	@ApiModelProperty(notes="校队人姓名",allowEmptyValue=true,example="",allowableValues="")
	String checkedUsername;

	
	@ApiModelProperty(notes="保密级别",allowEmptyValue=true,example="",allowableValues="")
	String secrecyLevel;

	
	@ApiModelProperty(notes="紧急程度",allowEmptyValue=true,example="",allowableValues="")
	String urgencyLevel;

	
	@ApiModelProperty(notes="主题词",allowEmptyValue=true,example="",allowableValues="")
	String topicName;

	
	@ApiModelProperty(notes="主题词Id",allowEmptyValue=true,example="",allowableValues="")
	String topicId;

	
	@ApiModelProperty(notes="标题",allowEmptyValue=true,example="",allowableValues="")
	String docTitle;

	
	@ApiModelProperty(notes="流程状态",allowEmptyValue=true,example="",allowableValues="")
	String docFlowState;

	
	@ApiModelProperty(notes="流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String docProcInstId;

	
	@ApiModelProperty(notes="文档编号=字名+（档案年度）+字号",allowEmptyValue=true,example="",allowableValues="")
	String docNo;

	
	@ApiModelProperty(notes="文档年月日yyyyMMdd",allowEmptyValue=true,example="",allowableValues="")
	String docYmd;

	
	@ApiModelProperty(notes="字名",allowEmptyValue=true,example="",allowableValues="")
	String wordName;

	
	@ApiModelProperty(notes="档案年度",allowEmptyValue=true,example="",allowableValues="")
	String docYear;

	
	@ApiModelProperty(notes="档案种类",allowEmptyValue=true,example="",allowableValues="")
	String docType;

	
	@ApiModelProperty(notes="机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="签报用户id",allowEmptyValue=true,example="",allowableValues="")
	String signUserid;

	
	@ApiModelProperty(notes="签报用户名称",allowEmptyValue=true,example="",allowableValues="")
	String signUsername;

	
	@ApiModelProperty(notes="签报用户部门id",allowEmptyValue=true,example="",allowableValues="")
	String signDeptid;

	
	@ApiModelProperty(notes="签报用户部门名称",allowEmptyValue=true,example="",allowableValues="")
	String signDeptname;

	
	@ApiModelProperty(notes="签报编号",allowEmptyValue=true,example="",allowableValues="")
	String signNumber;

	
	@ApiModelProperty(notes="签报标题",allowEmptyValue=true,example="",allowableValues="")
	String signTitle;

	
	@ApiModelProperty(notes="签报内容",allowEmptyValue=true,example="",allowableValues="")
	String signNav;

	
	@ApiModelProperty(notes="办公厅意见",allowEmptyValue=true,example="",allowableValues="")
	String officeOptions;

	
	@ApiModelProperty(notes="领导意见",allowEmptyValue=true,example="",allowableValues="")
	String leaderOptions;

	
	@ApiModelProperty(notes="文档种类，zffw:政府发文",allowEmptyValue=true,example="",allowableValues="")
	String docCategory;

	
	@ApiModelProperty(notes="来文机关id",allowEmptyValue=true,example="",allowableValues="")
	String comeDeptid;

	
	@ApiModelProperty(notes="来文机关名称",allowEmptyValue=true,example="",allowableValues="")
	String comeDeptname;

	
	@ApiModelProperty(notes="收文编号",allowEmptyValue=true,example="",allowableValues="")
	String receiveNumber;

	
	@ApiModelProperty(notes="文号",allowEmptyValue=true,example="",allowableValues="")
	String wenNumber;

	
	@ApiModelProperty(notes="拟办意见",allowEmptyValue=true,example="",allowableValues="")
	String nibanOptions;

	
	@ApiModelProperty(notes="领导批示",allowEmptyValue=true,example="",allowableValues="")
	String leaderComments;

	
	@ApiModelProperty(notes="批示的领导id",allowEmptyValue=true,example="",allowableValues="")
	String cleaderId;

	
	@ApiModelProperty(notes="批示的领导名称",allowEmptyValue=true,example="",allowableValues="")
	String cleaderName;

	
	@ApiModelProperty(notes="部门领导id",allowEmptyValue=true,example="",allowableValues="")
	String dleaderId;

	
	@ApiModelProperty(notes="部门领导名称",allowEmptyValue=true,example="",allowableValues="")
	String dleaderName;

	
	@ApiModelProperty(notes="收文的部门id",allowEmptyValue=true,example="",allowableValues="")
	String receiveDeptid;

	
	@ApiModelProperty(notes="收文的部门名称",allowEmptyValue=true,example="",allowableValues="")
	String receiveDeptname;

	
	@ApiModelProperty(notes="收文日期",allowEmptyValue=true,example="",allowableValues="")
	Date receiveDate;

	/**
	 *主键
	 **/
	public DocArchive(String docId) {
		this.docId = docId;
	}
    
    /**
     * 文档库
     **/
	public DocArchive() {
	}

}