package  com.mdp.arc.doc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 doc  小模块 DocWordName<br>
 */
@Data
@TableName("arc_doc_word_name")
@ApiModel(description="文档种类-作废-改为字典doc_word_name")
public class DocWordName  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="字名编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String wordId;

	
	@ApiModelProperty(notes="字名",allowEmptyValue=true,example="",allowableValues="")
	String wordName;

	
	@ApiModelProperty(notes="归属机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="归属部门编号",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	/**
	 *字名编号
	 **/
	public DocWordName(String wordId) {
		this.wordId = wordId;
	}
    
    /**
     * 文档种类-作废-改为字典doc_word_name
     **/
	public DocWordName() {
	}

}