package com.mdp.arc.doc.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.doc.entity.DocTopic;
import com.mdp.arc.doc.service.DocTopicService;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="/*/doc/docTopic")
@Api(tags={"文档主题-作废-改为字典-doc_topic-操作接口"})
public class DocTopicController {
	
	static Logger logger =LoggerFactory.getLogger(DocTopicController.class);
	
	@Autowired
	private DocTopicService docTopicService;

	@ApiOperation( value = "文档主题-作废-改为字典-doc_topic-查询列表",notes=" ")
	@ApiEntityParams(DocTopic.class)
	@ApiResponses({
		@ApiResponse(code = 200,response=DocTopic.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listDocTopic(@ApiIgnore @RequestParam Map<String,Object> params){
		try {
			User user=LoginUtils.getCurrentUserInfo();
			QueryWrapper<DocTopic> qw = QueryTools.initQueryWrapper(DocTopic.class , params);
			IPage page=QueryTools.initPage(params);
			List<Map<String,Object>> datas = docTopicService.selectListMapByWhere(page,qw,params);
			return Result.ok("query-ok","查询成功").setData(datas).setTotal(page.getTotal());
		}catch (BizException e) {
			return Result.error(e);
		}catch (Exception e) {
			return Result.error(e);
		}
	}
	

	@ApiOperation( value = "文档主题-作废-改为字典-doc_topic-新增",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocTopic.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addDocTopic(@RequestBody DocTopic docTopic) {
		 docTopicService.save(docTopic);
         return Result.ok("add-ok","添加成功！");
	}

	@ApiOperation( value = "文档主题-作废-改为字典-doc_topic-删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delDocTopic(@RequestBody DocTopic docTopic){
		docTopicService.removeById(docTopic);
        return Result.ok("del-ok","删除成功！");
	}

	@ApiOperation( value = "文档主题-作废-改为字典-doc_topic-修改",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocTopic.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editDocTopic(@RequestBody DocTopic docTopic) {
		docTopicService.updateById(docTopic);
        return Result.ok("edit-ok","修改成功！");
	}

    @ApiOperation( value = "文档主题-作废-改为字典-doc_topic-批量修改某些字段",notes="")
    @ApiEntityParams( value = DocTopic.class, props={ }, remark = "文档主题-作废-改为字典-doc_topic", paramType = "body" )
	@ApiResponses({
			@ApiResponse(code = 200,response=DocTopic.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/editSomeFields",method=RequestMethod.POST)
	public Result editSomeFields( @ApiIgnore @RequestBody Map<String,Object> params) {
        try{
            User user= LoginUtils.getCurrentUserInfo();
            docTopicService.editSomeFields(params);
            return Result.ok("edit-ok","更新成功");
        }catch (BizException e) {
            logger.error("",e);
            return Result.error(e);
        }catch (Exception e) {
            logger.error("",e);
            return Result.error(e);
        }
	}

	@ApiOperation( value = "文档主题-作废-改为字典-doc_topic-批量删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelDocTopic(@RequestBody List<DocTopic> docTopics) {
	    User user= LoginUtils.getCurrentUserInfo();
        try{ 
            if(docTopics.size()<=0){
                return Result.error("docTopic-batchDel-data-err-0","请上送待删除数据列表");
            }
             List<DocTopic> datasDb=docTopicService.listByIds(docTopics.stream().map(i-> i.getTopicId() ).collect(Collectors.toList()));

            List<DocTopic> can=new ArrayList<>();
            List<DocTopic> no=new ArrayList<>();
            for (DocTopic data : datasDb) {
                if(true){
                    can.add(data);
                }else{
                    no.add(data);
                } 
            }
            List<String> msgs=new ArrayList<>();
            if(can.size()>0){
                docTopicService.removeByIds(can);
                msgs.add(LangTips.transMsg("del-ok-num","成功删除%s条数据.",can.size()));
            }
    
            if(no.size()>0){ 
                msgs.add(LangTips.transMsg("not-allow-del-num","以下%s条数据不能删除:【%s】",no.size(),no.stream().map(i-> i.getTopicId() ).collect(Collectors.joining(","))));
            }
            if(can.size()>0){
                 return Result.ok(msgs.stream().collect(Collectors.joining()));
            }else {
                return Result.error(msgs.stream().collect(Collectors.joining()));
            } 
        }catch (BizException e) { 
           return Result.error(e);
        }catch (Exception e) {
            return Result.error(e);
        }


	} 

	@ApiOperation( value = "文档主题-作废-改为字典-doc_topic-根据主键查询一条数据",notes=" ")
     @ApiResponses({
            @ApiResponse(code = 200,response=DocTopic.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value="/queryById",method=RequestMethod.GET)
    public Result queryById(DocTopic docTopic) {
        DocTopic data = (DocTopic) docTopicService.getById(docTopic);
        return Result.ok().setData(data);
    }

	/**
	 * 查询主题分类并统计关联的公文数量
	 * @return
	 */
	@RequestMapping(value="/listAndCountType",method=RequestMethod.GET)
	public Map<String,Object> listAndCountType( @RequestParam Map<String,Object> params){
 		List<Map<String,Object>> datas = docTopicService.selectListAndCountType(params);	//列出DocType列表
		return Result.ok().setData(datas).put("count",datas.size());
	}

}
