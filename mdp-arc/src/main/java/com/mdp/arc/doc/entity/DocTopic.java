package  com.mdp.arc.doc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 doc  小模块 DocTopic<br>
 */
@Data
@TableName("arc_doc_topic")
@ApiModel(description="文档主题-作废-改为字典-doc_topic")
public class DocTopic  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主题编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String topicId;

	
	@ApiModelProperty(notes="主题名称",allowEmptyValue=true,example="",allowableValues="")
	String topicName;

	
	@ApiModelProperty(notes="备注说明",allowEmptyValue=true,example="",allowableValues="")
	String topicRemark;

	
	@ApiModelProperty(notes="机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="是否公共",allowEmptyValue=true,example="",allowableValues="")
	String isPub;

	/**
	 *主题编号
	 **/
	public DocTopic(String topicId) {
		this.topicId = topicId;
	}
    
    /**
     * 文档主题-作废-改为字典-doc_topic
     **/
	public DocTopic() {
	}

}