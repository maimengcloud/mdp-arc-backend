package com.mdp.arc.doc.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.doc.entity.DocOperlog;
import com.mdp.arc.doc.service.DocOperlogService;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import io.swagger.annotations.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * url编制采用rest风格,如对ARC.arc_doc_operlog arc_doc_operlog的操作有增删改查,对应的url分别为:<br>
 *  新增: doc/docOperlog/add <br>
 *  查询: doc/docOperlog/list<br>
 *  模糊查询: doc/docOperlog/listKey<br>
 *  修改: doc/docOperlog/edit <br>
 *  删除: doc/docOperlog/del<br>
 *  批量删除: doc/docOperlog/batchDel<br>
 * 组织 com.qqkj  顶级模块 mdp.arc 大模块 doc 小模块 <br>
 * 实体 DocOperlog 表 ARC.arc_doc_operlog 当前主键(包括多主键): id; 
 ***/
@RestController("mdp.arc.doc.docOperlogController")
@RequestMapping(value="/*/doc/docOperlog")
@Api(tags={"arc_doc_operlog操作接口"})
public class DocOperlogController {
	
	static Log logger=LogFactory.getLog(DocOperlogController.class);
	
	@Autowired
	private DocOperlogService docOperlogService;
	 
		
 
	
	@ApiOperation( value = "查询arc_doc_operlog信息列表",notes="listDocOperlog,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
	@ApiImplicitParams({  
		@ApiImplicitParam(name="id",value="日志编号,主键",required=false),
		@ApiImplicitParam(name="docNo",value="文档编号",required=false),
		@ApiImplicitParam(name="operUserid",value="操作者编号",required=false),
		@ApiImplicitParam(name="operUsername",value="操作者姓名",required=false),
		@ApiImplicitParam(name="operDate",value="操作时间",required=false),
		@ApiImplicitParam(name="operType",value="操作类型read阅读/download下载",required=false),
		@ApiImplicitParam(name="clientIp",value="客户端ip地址",required=false),
		@ApiImplicitParam(name="docTitle",value="文档标题",required=false),
		@ApiImplicitParam(name="remark",value="备注",required=false),
		@ApiImplicitParam(name="opBranchId",value="操作者归属机构号",required=false),
		@ApiImplicitParam(name="docId",value="主键",required=false),
		@ApiImplicitParam(name="pageSize",value="每页记录数",required=false),
		@ApiImplicitParam(name="currentPage",value="当前页码,从1开始",required=false),
		@ApiImplicitParam(name="total",value="总记录数,服务器端收到0时，会自动计算总记录数，如果上传>0的不自动计算",required=false),
		@ApiImplicitParam(name="orderFields",value="排序列 如性别、学生编号排序 ['sex','studentId']",required=false),
		@ApiImplicitParam(name="orderDirs",value="排序方式,与orderFields对应，升序 asc,降序desc 如 性别 升序、学生编号降序 ['asc','desc']",required=false) 
	})
	@ApiResponses({
		@ApiResponse(code = 200,response= DocOperlog.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listDocOperlog(@RequestParam Map<String,Object> params ){
		 
		RequestUtils.transformArray(params, "ids");
		IPage page= QueryTools.initPage(params);
		List<Map<String,Object>>	datas = docOperlogService.selectListMapByWhere(page,QueryTools.initQueryWrapper(DocOperlog.class,params),params);	//列出DocOperlog列表
		
		return Result.ok().setData(datas).setTotal(page.getTotal());
	}
	
 
	
	/**
	@ApiOperation( value = "新增一条arc_doc_operlog信息",notes="addDocOperlog,主键如果为空，后台自动生成")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocOperlog.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addDocOperlog(@RequestBody DocOperlog docOperlog) {
		
		
		try{
			if(StringUtils.isEmpty(docOperlog.getId())) {
				docOperlog.setId(docOperlogService.createKey("id"));
			}else{
				 DocOperlog docOperlogQuery = new  DocOperlog(docOperlog.getId());
				if(docOperlogService.countByWhere(docOperlogQuery)>0){
					return Result.error("编号重复，请修改编号再提交");
					
				}
			}
			docOperlogService.insert(docOperlog);
			return Result.ok().setData(docOperlog);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	*/
	
	/**
	@ApiOperation( value = "删除一条arc_doc_operlog信息",notes="delDocOperlog,仅需要上传主键字段")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delDocOperlog(@RequestBody DocOperlog docOperlog){
		
		
		try{
			docOperlogService.deleteByPk(docOperlog);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	 */
	
	/**
	@ApiOperation( value = "根据主键修改一条arc_doc_operlog信息",notes="editDocOperlog")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocOperlog.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editDocOperlog(@RequestBody DocOperlog docOperlog) {
		
		
		try{
			docOperlogService.updateByPk(docOperlog);
			return Result.ok().setData(docOperlog);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	*/
	

	
	/**
	@ApiOperation( value = "根据主键列表批量删除arc_doc_operlog信息",notes="batchDelDocOperlog,仅需要上传主键字段")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelDocOperlog(@RequestBody List<DocOperlog> docOperlogs) {
		
		
		try{ 
			docOperlogService.batchDelete(docOperlogs);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	} 
	*/
}
