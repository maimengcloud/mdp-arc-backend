package  com.mdp.arc.doc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 doc  小模块 DocOperlog<br>
 */
@Data
@TableName("arc_doc_operlog")
@ApiModel(description="文档操作日志")
public class DocOperlog  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="日志编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="文档编号",allowEmptyValue=true,example="",allowableValues="")
	String docNo;

	
	@ApiModelProperty(notes="操作者编号",allowEmptyValue=true,example="",allowableValues="")
	String operUserid;

	
	@ApiModelProperty(notes="操作者姓名",allowEmptyValue=true,example="",allowableValues="")
	String operUsername;

	
	@ApiModelProperty(notes="操作时间",allowEmptyValue=true,example="",allowableValues="")
	Date operDate;

	
	@ApiModelProperty(notes="操作类型read阅读/download下载",allowEmptyValue=true,example="",allowableValues="")
	String operType;

	
	@ApiModelProperty(notes="客户端ip地址",allowEmptyValue=true,example="",allowableValues="")
	String clientIp;

	
	@ApiModelProperty(notes="文档标题",allowEmptyValue=true,example="",allowableValues="")
	String docTitle;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="操作者归属机构号",allowEmptyValue=true,example="",allowableValues="")
	String opBranchId;

	
	@ApiModelProperty(notes="主键",allowEmptyValue=true,example="",allowableValues="")
	String docId;

	/**
	 *日志编号
	 **/
	public DocOperlog(String id) {
		this.id = id;
	}
    
    /**
     * 文档操作日志
     **/
	public DocOperlog() {
	}

}