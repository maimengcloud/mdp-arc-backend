package com.mdp.arc.doc.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.doc.entity.DocWordName;
import com.mdp.arc.doc.service.DocWordNameService;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="/*/doc/docWordName")
@Api(tags={"文档种类-作废-改为字典doc_word_name-操作接口"})
public class DocWordNameController {
	
	static Logger logger =LoggerFactory.getLogger(DocWordNameController.class);
	
	@Autowired
	private DocWordNameService docWordNameService;

	@ApiOperation( value = "文档种类-作废-改为字典doc_word_name-查询列表",notes=" ")
	@ApiEntityParams(DocWordName.class)
	@ApiResponses({
		@ApiResponse(code = 200,response=DocWordName.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listDocWordName(@ApiIgnore @RequestParam Map<String,Object> params){
		try {
			User user=LoginUtils.getCurrentUserInfo();
			QueryWrapper<DocWordName> qw = QueryTools.initQueryWrapper(DocWordName.class , params);
			IPage page=QueryTools.initPage(params);
			List<Map<String,Object>> datas = docWordNameService.selectListMapByWhere(page,qw,params);
			return Result.ok("query-ok","查询成功").setData(datas).setTotal(page.getTotal());
		}catch (BizException e) {
			return Result.error(e);
		}catch (Exception e) {
			return Result.error(e);
		}
	}
	

	@ApiOperation( value = "文档种类-作废-改为字典doc_word_name-新增",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocWordName.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addDocWordName(@RequestBody DocWordName docWordName) {
		 docWordNameService.save(docWordName);
         return Result.ok("add-ok","添加成功！");
	}

	@ApiOperation( value = "文档种类-作废-改为字典doc_word_name-删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delDocWordName(@RequestBody DocWordName docWordName){
		docWordNameService.removeById(docWordName);
        return Result.ok("del-ok","删除成功！");
	}

	@ApiOperation( value = "文档种类-作废-改为字典doc_word_name-修改",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocWordName.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editDocWordName(@RequestBody DocWordName docWordName) {
		docWordNameService.updateById(docWordName);
        return Result.ok("edit-ok","修改成功！");
	}

    @ApiOperation( value = "文档种类-作废-改为字典doc_word_name-批量修改某些字段",notes="")
    @ApiEntityParams( value = DocWordName.class, props={ }, remark = "文档种类-作废-改为字典doc_word_name", paramType = "body" )
	@ApiResponses({
			@ApiResponse(code = 200,response=DocWordName.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/editSomeFields",method=RequestMethod.POST)
	public Result editSomeFields( @ApiIgnore @RequestBody Map<String,Object> params) {
        try{
            User user= LoginUtils.getCurrentUserInfo();
            docWordNameService.editSomeFields(params);
            return Result.ok("edit-ok","更新成功");
        }catch (BizException e) {
            logger.error("",e);
            return Result.error(e);
        }catch (Exception e) {
            logger.error("",e);
            return Result.error(e);
        }
	}

	@ApiOperation( value = "文档种类-作废-改为字典doc_word_name-批量删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelDocWordName(@RequestBody List<DocWordName> docWordNames) {
	    User user= LoginUtils.getCurrentUserInfo();
        try{ 
            if(docWordNames.size()<=0){
                return Result.error("docWordName-batchDel-data-err-0","请上送待删除数据列表");
            }
             List<DocWordName> datasDb=docWordNameService.listByIds(docWordNames.stream().map(i-> i.getWordId() ).collect(Collectors.toList()));

            List<DocWordName> can=new ArrayList<>();
            List<DocWordName> no=new ArrayList<>();
            for (DocWordName data : datasDb) {
                if(true){
                    can.add(data);
                }else{
                    no.add(data);
                } 
            }
            List<String> msgs=new ArrayList<>();
            if(can.size()>0){
                docWordNameService.removeByIds(can);
                msgs.add(LangTips.transMsg("del-ok-num","成功删除%s条数据.",can.size()));
            }
    
            if(no.size()>0){ 
                msgs.add(LangTips.transMsg("not-allow-del-num","以下%s条数据不能删除:【%s】",no.size(),no.stream().map(i-> i.getWordId() ).collect(Collectors.joining(","))));
            }
            if(can.size()>0){
                 return Result.ok(msgs.stream().collect(Collectors.joining()));
            }else {
                return Result.error(msgs.stream().collect(Collectors.joining()));
            } 
        }catch (BizException e) { 
           return Result.error(e);
        }catch (Exception e) {
            return Result.error(e);
        }


	} 

	@ApiOperation( value = "文档种类-作废-改为字典doc_word_name-根据主键查询一条数据",notes=" ")
     @ApiResponses({
            @ApiResponse(code = 200,response=DocWordName.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value="/queryById",method=RequestMethod.GET)
    public Result queryById(DocWordName docWordName) {
        DocWordName data = (DocWordName) docWordNameService.getById(docWordName);
        return Result.ok().setData(data);
    }

}
