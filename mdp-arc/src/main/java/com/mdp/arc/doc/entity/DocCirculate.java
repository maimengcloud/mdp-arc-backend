package  com.mdp.arc.doc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 doc  小模块 DocCirculate<br>
 */
@Data
@TableName("arc_doc_circulate")
@ApiModel(description="传阅对象")
public class DocCirculate  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="传阅对象类型dept部门/user用户",allowEmptyValue=true,example="",allowableValues="")
	String targetType;

	
	@ApiModelProperty(notes="传阅对象编号",allowEmptyValue=true,example="",allowableValues="")
	String targetId;

	
	@ApiModelProperty(notes="传阅对象名称",allowEmptyValue=true,example="",allowableValues="")
	String targetName;

	
	@ApiModelProperty(notes="文档编号",allowEmptyValue=true,example="",allowableValues="")
	String docNo;

	
	@ApiModelProperty(notes="read读/edit编辑/full完全控制",allowEmptyValue=true,example="",allowableValues="")
	String authority;

	
	@ApiModelProperty(notes="是否可下载",allowEmptyValue=true,example="",allowableValues="")
	String canDownload;

	
	@ApiModelProperty(notes="传阅时间",allowEmptyValue=true,example="",allowableValues="")
	Date readTime;

	
	@ApiModelProperty(notes="归属机构",allowEmptyValue=true,example="",allowableValues="")
	String targetBranchId;

	
	@ApiModelProperty(notes="档案编号",allowEmptyValue=true,example="",allowableValues="")
	String docId;

	/**
	 *主键
	 **/
	public DocCirculate(String id) {
		this.id = id;
	}
    
    /**
     * 传阅对象
     **/
	public DocCirculate() {
	}

}