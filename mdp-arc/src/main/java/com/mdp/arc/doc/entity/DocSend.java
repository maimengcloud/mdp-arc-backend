package  com.mdp.arc.doc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 doc  小模块 DocSend<br>
 */
@Data
@TableName("arc_doc_send")
@ApiModel(description="发文")
public class DocSend  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String docId;

	
	@ApiModelProperty(notes="发送时间",allowEmptyValue=true,example="",allowableValues="")
	Date sendDate;

	
	@ApiModelProperty(notes="发送人",allowEmptyValue=true,example="",allowableValues="")
	String sendUserid;

	
	@ApiModelProperty(notes="发送人姓名",allowEmptyValue=true,example="",allowableValues="")
	String sendUsername;

	
	@ApiModelProperty(notes="发送部门",allowEmptyValue=true,example="",allowableValues="")
	String sendDeptid;

	
	@ApiModelProperty(notes="发送部门名称",allowEmptyValue=true,example="",allowableValues="")
	String sendDeptName;

	
	@ApiModelProperty(notes="发送机构号",allowEmptyValue=true,example="",allowableValues="")
	String sendBranchId;

	
	@ApiModelProperty(notes="文档编号",allowEmptyValue=true,example="",allowableValues="")
	String docNo;

	
	@ApiModelProperty(notes="流程状态",allowEmptyValue=true,example="",allowableValues="")
	String sendFlowState;

	
	@ApiModelProperty(notes="流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String sendProcInstId;

	
	@ApiModelProperty(notes="签章动作人",allowEmptyValue=true,example="",allowableValues="")
	String signedUserid;

	
	@ApiModelProperty(notes="签章单位",allowEmptyValue=true,example="",allowableValues="")
	String signedDeptid;

	
	@ApiModelProperty(notes="签章动作人姓名",allowEmptyValue=true,example="",allowableValues="")
	String signedUsername;

	
	@ApiModelProperty(notes="签章单位名",allowEmptyValue=true,example="",allowableValues="")
	String signedDeptName;

	
	@ApiModelProperty(notes="印章编号",allowEmptyValue=true,example="",allowableValues="")
	String signedId;

	/**
	 *主键
	 **/
	public DocSend(String docId) {
		this.docId = docId;
	}
    
    /**
     * 发文
     **/
	public DocSend() {
	}

}