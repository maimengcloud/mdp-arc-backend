package com.mdp.arc.doc.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.doc.entity.DocArchive;
import com.mdp.arc.doc.entity.DocCirculate;
import com.mdp.arc.doc.entity.DocSend;
import com.mdp.arc.doc.entity.DocSendTarget;
import com.mdp.arc.doc.mapper.DocArchiveMapper;
import com.mdp.arc.doc.po.DocArchiveComeVo;
import com.mdp.arc.doc.po.DocArchiveVo;
import com.mdp.core.dao.SequenceDao;
import com.mdp.core.query.QueryTools;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 父类已经支持增删改查操作,因此,即使本类什么也不写,也已经可以满足一般的增删改查操作了.<br> 
 * 组织 com.mdp  顶级模块 arc 大模块 doc 小模块 <br>
 * 实体 DocArchive 表 arc_doc_archive 当前主键(包括多主键): doc_id; 
 ***/
@Service
public class DocArchiveService extends BaseService<DocArchiveMapper,DocArchive> {
	static Logger logger =LoggerFactory.getLogger(DocArchiveService.class);


	/** 请在此类添加自定义函数 */
	SequenceDao sequenceDao;
	@Autowired
	DataSource dataSource;

	@Autowired
	private DocSendTargetService docSendTargetService;

	@Autowired
	private DocSendService docSendService;

	@Autowired
	private DocCirculateService docCirculateService;

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
	private Calendar calendar = Calendar.getInstance();

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}

	/**
	 * 插入政府发文公文
	 * @param
	 */
	@Transactional
	public void insertDocArchive(DocArchiveVo docArchiveVo) {
		//获取当前登录人对象
		User user = LoginUtils.getCurrentUserInfo();
		DocArchive voDocArchive = docArchiveVo.getDocArchive();
		DocSend voDocSend = docArchiveVo.getDocSend();
		List<DocSendTarget> voDocSendTargets = docArchiveVo.getDocSendTargets();
		//生成档案年度
		String docYear = simpleDateFormat.format(calendar.getTime()).substring(0,4);
		//生成文档编号
		String docNum = voDocArchive.getWordNum() + docYear + voDocArchive.getWordName();

		//1.组合数据，插入文档库表
		DocArchive docArchive = new DocArchive();
		BeanUtils.copyProperties(voDocArchive, docArchive);
		//设置其他参数
		//writtenDate 成文日期
		docArchive.setWrittenDate(new Date());
		docArchive.setDocYear(docYear);
		docArchive.setDocNo(docNum);
		docArchive.setDocYmd(simpleDateFormat.format(calendar.getTime()));
		//设置公文库公文分类
		docArchive.setDocCategory("zffw");
		//TODO:
		//流程状态 + 流程状态实例
		this.insert(docArchive);

		//2.组合数据，插入发文表
		DocSend docSend = new DocSend();
		BeanUtils.copyProperties(voDocSend, docSend);
		docSend.setDocNo(docNum);
		//设置发送人信息
		docSend.setSendDate(new Date());
		docSend.setSendUserid(user.getUserid());
		docSend.setSendUsername(user.getUsername());
		docSend.setSendDeptid(user.getDeptid());
		docSend.setSendDeptName(user.getDeptName());
		docSend.setSendBranchId(user.getBranchId());
		docSendService.insert(docSend);

		//3.组合数据，插入发文对象表
		List<DocSendTarget> docSendTargetList = new ArrayList<>();
		for(DocSendTarget docSendTarget: voDocSendTargets) {
			DocSendTarget insertData = new DocSendTarget();
			BeanUtils.copyProperties(docSendTarget, insertData);
			insertData.setDocNo(docNum);
			insertData.setDocId(voDocArchive.getDocId());
			insertData.setId(docSendTargetService.createKey("id"));
			insertData.setTargetBranchId(voDocArchive.getBranchId());
			docSendTargetList.add(insertData);
		}
		docSendTargetService.batchInsert(docSendTargetList);
	}

	/**
	 * 添加工作签报
	 * @param docArchiveVo
	 */
	public void insertWorkSign(DocArchiveVo docArchiveVo) {
		//获取文档对象
		DocArchive voDocArchive = docArchiveVo.getDocArchive();
		//生成档案年度
		String docYear = simpleDateFormat.format(calendar.getTime()).substring(0,4);
		//生成文档编号
		String docNum = voDocArchive.getWordNum() + docYear + voDocArchive.getWordName();
		//1.组合数据，插入文档库表
		DocArchive docArchive = new DocArchive();
		BeanUtils.copyProperties(voDocArchive, docArchive);
		//设置其他参数
		//writtenDate 成文日期,档案年度,文档编号
		docArchive.setWrittenDate(new Date());
		docArchive.setDocYear(docYear);
		docArchive.setDocNo(docNum);
		docArchive.setDocYmd(simpleDateFormat.format(calendar.getTime()));
		//设置公文库公文分类
		docArchive.setDocCategory("gzqb");
		//设置发文日期
		docArchive.setWrittenDate(docArchive.getDispatchDate());
		this.insert(docArchive);
	}

	/**
	 * 创建收文记录
	 * @param docArchiveComeVo
	 */
	@Transactional
	public void addRegistration(DocArchiveComeVo docArchiveComeVo) {
		//获取文档对象
		DocArchive voDocArchive = docArchiveComeVo.getDocArchive();
		//生成档案年度
		String docYear = simpleDateFormat.format(calendar.getTime()).substring(0,4);
		//生成文档编号
		String docNum = voDocArchive.getWordNum() + docYear + voDocArchive.getWordName();
		//1.组合数据，插入文档库表
		DocArchive docArchive = new DocArchive();
		BeanUtils.copyProperties(voDocArchive, docArchive);
		//设置其他参数
		//writtenDate 成文日期,档案年度,文档编号
		docArchive.setWrittenDate(new Date());
		docArchive.setDocYear(docYear);
		docArchive.setDocNo(docNum);
		docArchive.setDocYmd(simpleDateFormat.format(calendar.getTime()));
		//设置公文库公文分类
		this.insert(docArchive);
		//2.获取传阅对象集合
		List<DocCirculate> docCirculates = docArchiveComeVo.getDocCirculates();
		List<DocCirculate> insertParams  = new ArrayList<>();
		for (DocCirculate docCirculate : docCirculates) {
			DocCirculate target = new DocCirculate();
			BeanUtils.copyProperties(docCirculate, target);
			target.setTargetBranchId(voDocArchive.getBranchId());
			target.setId(docCirculateService.createKey("id"));
			target.setDocId(voDocArchive.getDocId());
			target.setDocNo(docNum);
			insertParams.add(target);
		}
		if (insertParams.size() <= 0) return;
		docCirculateService.batchInsert(insertParams);
	}


	public String createWordNum(){
		String seq=  sequenceDao.generate("word_num","档案字号",4,"1","","",new Date());
		return seq;
	}

	public List<Map<String, Object>> selectEightByWhere(Map<String, Object> docArchive) {
		docArchive.put("pageNum",1);
		docArchive.put("pageSize",8);
		return this.selectListMapByWhere(QueryTools.initPage(docArchive),QueryTools.initQueryWrapper(DocArchive.class,docArchive),docArchive);
	}
	@PostConstruct
	public void init(){
		this.sequenceDao=new SequenceDao();
		this.sequenceDao.setDataSource(dataSource);
	}
}

