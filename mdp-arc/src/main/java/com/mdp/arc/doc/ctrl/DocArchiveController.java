package com.mdp.arc.doc.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.doc.entity.DocArchive;
import com.mdp.arc.doc.po.DocArchiveComeVo;
import com.mdp.arc.doc.po.DocArchiveVo;
import com.mdp.arc.doc.service.DocArchiveService;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.swagger.ApiEntityParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * url编制采用rest风格,如对ARC.arc_doc_archive arc_doc_archive的操作有增删改查,对应的url分别为:<br>
 *  新增: doc/docArchive/add <br>
 *  查询: doc/docArchive/list<br>
 *  模糊查询: doc/docArchive/listKey<br>
 *  修改: doc/docArchive/edit <br>
 *  删除: doc/docArchive/del<br>
 *  批量删除: doc/docArchive/batchDel<br>
 * 组织 com.qqkj  顶级模块 mdp.arc 大模块 doc 小模块 <br>
 * 实体 DocArchive 表 ARC.arc_doc_archive 当前主键(包括多主键): doc_id; 
 ***/
@RestController("mdp.arc.doc.docArchiveController")
@RequestMapping(value="/*/doc/docArchive")
@Api(tags={"arc_doc_archive操作接口"})
public class DocArchiveController {
	
	static Log logger=LogFactory.getLog(DocArchiveController.class);
	
	@Autowired
	private DocArchiveService docArchiveService;
	 
		
 
	
	@ApiOperation( value = "查询arc_doc_archive信息列表",notes="listDocArchive,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
	@ApiEntityParams(DocArchive.class)
	@ApiResponses({
		@ApiResponse(code = 200,response= DocArchive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listDocArchive( @RequestParam Map<String,Object> params ){
		 
		RequestUtils.transformArray(params, "docIds");
		IPage page= QueryTools.initPage(params); 
		List<Map<String,Object>>	datas = docArchiveService.selectListMapByWhere(page,QueryTools.initQueryWrapper(DocArchive.class,params),params);	//列出DocArchive列表
 		
		return Result.ok().setData(datas);
	}

	@ApiOperation( value = "新增一条arc_doc_archive信息",notes="addDocArchive,主键如果为空，后台自动生成")
	@ApiResponses({
			@ApiResponse(code = 200,response=DocArchive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addDocArchive(@RequestBody DocArchiveVo docArchiveVo) {
		
		
		try{
			//插入数据
			if(StringUtils.isEmpty(docArchiveVo.getDocArchive().getDocId())) {
				docArchiveVo.getDocArchive().setDocNo(docArchiveService.createKey("docId"));
			}else{
				DocArchive docArchiveQuery = new  DocArchive(docArchiveVo.getDocArchive().getDocId());
				if(docArchiveService.countByWhere(docArchiveQuery)>0){
					return Result.error("编号重复，请修改编号再提交");
					
				}
			}
			String wordNum = docArchiveService.createWordNum();
			docArchiveVo.getDocArchive().setWordNum(wordNum);
			docArchiveService.insertDocArchive(docArchiveVo);
			return Result.ok().setData( docArchiveVo);
		}catch (BizException e) { 
			logger.error("",e);
			return Result.error(e);
		}catch (Exception e) { 
			logger.error("",e);
			return Result.error(e.getMessage());
		}
	}


	@ApiOperation( value = "新增一条arc_doc_archive信息(工作签报)",notes="addDocArchive,主键如果为空，后台自动生成")
	@ApiResponses({
			@ApiResponse(code = 200,response=DocArchive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/addWorkSign",method=RequestMethod.POST)
	public Result addWorkSign(@RequestBody DocArchiveVo docArchiveVo) { 
		try{
			//插入数据
			if(StringUtils.isEmpty(docArchiveVo.getDocArchive().getDocId())) {
				docArchiveVo.getDocArchive().setDocNo(docArchiveService.createKey("docId"));
			}else{
				DocArchive docArchiveQuery = new  DocArchive(docArchiveVo.getDocArchive().getDocId());
				if(docArchiveService.countByWhere(docArchiveQuery)>0){
					return Result.error("编号重复，请修改编号再提交");
					
				}
			}
			String wordNum = docArchiveService.createWordNum();
			docArchiveVo.getDocArchive().setWordNum(wordNum);
			System.out.println(docArchiveVo.toString() + "==================================>");
			docArchiveService.insertWorkSign(docArchiveVo);
		}catch (BizException e) { 
			logger.error("",e);
			return Result.error(e);
		}catch (Exception e) { 
			logger.error("",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}


	@ApiOperation( value = "新增一条arc_doc_archive信息(收文登记)",notes="addDocArchive,主键如果为空，后台自动生成")
	@ApiResponses({
			@ApiResponse(code = 200,response=DocArchive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/addRegistration",method=RequestMethod.POST)
	public Result addRegistration(@RequestBody DocArchiveComeVo docArchiveComeVo) {
		Map<String, Object> m = new HashMap<>();
		Tips tips = new Tips("成功新增一条数据");
		try {
			//插入数据
			if (StringUtils.isEmpty(docArchiveComeVo.getDocArchive().getDocId())) {
				docArchiveComeVo.getDocArchive().setDocNo(docArchiveService.createKey("docId"));
			} else {
				DocArchive docArchiveQuery = new DocArchive(docArchiveComeVo.getDocArchive().getDocId());
				if (docArchiveService.countByWhere(docArchiveQuery) > 0) {
					return Result.error("编号重复，请修改编号再提交");
					
				}
			}
			String wordNum = docArchiveService.createWordNum();
			docArchiveComeVo.getDocArchive().setWordNum(wordNum);
			docArchiveService.addRegistration(docArchiveComeVo);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}




		/**
         * 查询八条最新数据用于首页展示
         * @return
         */
	@RequestMapping(value="/selectEightDocArchive",method=RequestMethod.GET)
	public Result selectEightDocArchive( @RequestParam Map<String,Object> params ){
		
		RequestUtils.transformArray(params, "docNos");
		IPage page= QueryTools.initPage(params);
		List<Map<String,Object>>	datas = docArchiveService.selectEightByWhere(params);	//列出DocArchive列表
		
		return Result.ok().setData(datas).setTotal(datas.size());

	}


	/**
	@ApiOperation( value = "新增一条arc_doc_archive信息",notes="addDocArchive,主键如果为空，后台自动生成")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocArchive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addDocArchive(@RequestBody DocArchive docArchive) {
		
		
		try{
			if(StringUtils.isEmpty(docArchive.getDocId())) {
				docArchive.setDocId(docArchiveService.createKey("docId"));
			}else{
				 DocArchive docArchiveQuery = new  DocArchive(docArchive.getDocId());
				if(docArchiveService.countByWhere(docArchiveQuery)>0){
					return Result.error("编号重复，请修改编号再提交");
					
				}
			}
			docArchiveService.insert(docArchive);
			return Result.ok().setData(docArchive);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	*/
	
	/**
	@ApiOperation( value = "删除一条arc_doc_archive信息",notes="delDocArchive,仅需要上传主键字段")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delDocArchive(@RequestBody DocArchive docArchive){
		
		
		try{
			docArchiveService.deleteByPk(docArchive);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	 */
	
	/**
	@ApiOperation( value = "根据主键修改一条arc_doc_archive信息",notes="editDocArchive")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocArchive.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editDocArchive(@RequestBody DocArchive docArchive) {
		
		
		try{
			docArchiveService.updateByPk(docArchive);
			return Result.ok().setData(docArchive);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	*/
	

	
	/**
	@ApiOperation( value = "根据主键列表批量删除arc_doc_archive信息",notes="batchDelDocArchive,仅需要上传主键字段")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelDocArchive(@RequestBody List<DocArchive> docArchives) {
		
		
		try{ 
			docArchiveService.batchDelete(docArchives);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	} 
	*/
}
