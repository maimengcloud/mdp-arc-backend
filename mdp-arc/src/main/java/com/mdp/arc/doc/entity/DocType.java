package  com.mdp.arc.doc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 doc  小模块 DocType<br>
 */
@Data
@TableName("arc_doc_type")
@ApiModel(description="文档种类-作废-改为字典doc_type")
public class DocType  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="类型编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String typeId;

	
	@ApiModelProperty(notes="类型名称",allowEmptyValue=true,example="",allowableValues="")
	String typeName;

	
	@ApiModelProperty(notes="类型备注",allowEmptyValue=true,example="",allowableValues="")
	String typeRemark;

	
	@ApiModelProperty(notes="归属机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="是否公共",allowEmptyValue=true,example="",allowableValues="")
	String isPub;

	/**
	 *类型编号
	 **/
	public DocType(String typeId) {
		this.typeId = typeId;
	}
    
    /**
     * 文档种类-作废-改为字典doc_type
     **/
	public DocType() {
	}

}