package com.mdp.arc.doc.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.doc.entity.DocCirculate;
import com.mdp.arc.doc.service.DocCirculateService;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import io.swagger.annotations.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
/**
 * url编制采用rest风格,如对ARC.arc_doc_circulate arc_doc_circulate的操作有增删改查,对应的url分别为:<br>
 *  新增: doc/docCirculate/add <br>
 *  查询: doc/docCirculate/list<br>
 *  模糊查询: doc/docCirculate/listKey<br>
 *  修改: doc/docCirculate/edit <br>
 *  删除: doc/docCirculate/del<br>
 *  批量删除: doc/docCirculate/batchDel<br>
 * 组织 com.qqkj  顶级模块 mdp.arc 大模块 doc 小模块 <br>
 * 实体 DocCirculate 表 ARC.arc_doc_circulate 当前主键(包括多主键): id; 
 ***/
@RestController("mdp.arc.doc.docCirculateController")
@RequestMapping(value="/*/doc/docCirculate")
@Api(tags={"arc_doc_circulate操作接口"})
public class DocCirculateController {
	
	static Log logger=LogFactory.getLog(DocCirculateController.class);
	
	@Autowired
	private DocCirculateService docCirculateService;
	 
		
 
	
	@ApiOperation( value = "查询arc_doc_circulate信息列表",notes="listDocCirculate,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
	@ApiImplicitParams({  
		@ApiImplicitParam(name="id",value="主键,主键",required=false),
		@ApiImplicitParam(name="targetType",value="传阅对象类型dept部门/user用户",required=false),
		@ApiImplicitParam(name="targetId",value="传阅对象编号",required=false),
		@ApiImplicitParam(name="targetName",value="传阅对象名称",required=false),
		@ApiImplicitParam(name="docNo",value="文档编号",required=false),
		@ApiImplicitParam(name="authority",value="read读/edit编辑/full完全控制",required=false),
		@ApiImplicitParam(name="canDownload",value="是否可下载",required=false),
		@ApiImplicitParam(name="readTime",value="传阅时间",required=false),
		@ApiImplicitParam(name="targetBranchId",value="归属机构",required=false),
		@ApiImplicitParam(name="docId",value="档案编号",required=false),
		@ApiImplicitParam(name="pageSize",value="每页记录数",required=false),
		@ApiImplicitParam(name="currentPage",value="当前页码,从1开始",required=false),
		@ApiImplicitParam(name="total",value="总记录数,服务器端收到0时，会自动计算总记录数，如果上传>0的不自动计算",required=false),
		@ApiImplicitParam(name="orderFields",value="排序列 如性别、学生编号排序 ['sex','studentId']",required=false),
		@ApiImplicitParam(name="orderDirs",value="排序方式,与orderFields对应，升序 asc,降序desc 如 性别 升序、学生编号降序 ['asc','desc']",required=false) 
	})
	@ApiResponses({
		@ApiResponse(code = 200,response=DocCirculate.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listDocCirculate(@RequestParam Map<String,Object> params ){
		 
		RequestUtils.transformArray(params, "ids");
		IPage page= QueryTools.initPage(params);
		List<Map<String,Object>>	datas = docCirculateService.selectListMapByWhere(page,QueryTools.initQueryWrapper(DocCirculate.class,params),params);	//列出DocCirculate列表
		
		return Result.ok().setData(datas).setTotal(page.getTotal());
	}
	
 
	
	/**
	@ApiOperation( value = "新增一条arc_doc_circulate信息",notes="addDocCirculate,主键如果为空，后台自动生成")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocCirculate.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addDocCirculate(@RequestBody DocCirculate docCirculate) {
		
		
		try{
			if(StringUtils.isEmpty(docCirculate.getId())) {
				docCirculate.setId(docCirculateService.createKey("id"));
			}else{
				 DocCirculate docCirculateQuery = new  DocCirculate(docCirculate.getId());
				if(docCirculateService.countByWhere(docCirculateQuery)>0){
					return Result.error("编号重复，请修改编号再提交");
					
				}
			}
			docCirculateService.insert(docCirculate);
			return Result.ok().setData(docCirculate);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	*/
	
	/**
	@ApiOperation( value = "删除一条arc_doc_circulate信息",notes="delDocCirculate,仅需要上传主键字段")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delDocCirculate(@RequestBody DocCirculate docCirculate){
		
		
		try{
			docCirculateService.deleteByPk(docCirculate);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	 */
	
	/**
	@ApiOperation( value = "根据主键修改一条arc_doc_circulate信息",notes="editDocCirculate")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocCirculate.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editDocCirculate(@RequestBody DocCirculate docCirculate) {
		
		
		try{
			docCirculateService.updateByPk(docCirculate);
			return Result.ok().setData(docCirculate);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	}
	*/
	

	
	/**
	@ApiOperation( value = "根据主键列表批量删除arc_doc_circulate信息",notes="batchDelDocCirculate,仅需要上传主键字段")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelDocCirculate(@RequestBody List<DocCirculate> docCirculates) {
		
		
		try{ 
			docCirculateService.batchDelete(docCirculates);
		}catch (BizException e) {
		logger.error("",e);
		return Result.error(e);
	}catch (Exception e) {
		logger.error("",e);
		return Result.error(e.getMessage());

	}  
		return Result.ok();
	} 
	*/
}
