package com.mdp.arc.doc.po;


import com.mdp.arc.doc.entity.DocArchive;
import com.mdp.arc.doc.entity.DocSend;
import com.mdp.arc.doc.entity.DocSendTarget;
import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel(description="arc_doc_archive_po")
public class DocArchiveVo {

    private DocArchive docArchive;

    private DocSend docSend;

    private List<DocSendTarget> docSendTargets;


    public DocArchive getDocArchive() {
        return docArchive;
    }

    public void setDocArchive(DocArchive docArchive) {
        this.docArchive = docArchive;
    }

    public DocSend getDocSend() {
        return docSend;
    }

    public void setDocSend(DocSend docSend) {
        this.docSend = docSend;
    }

    public List<DocSendTarget> getDocSendTargets() {
        return docSendTargets;
    }

    public void setDocSendTargets(List<DocSendTarget> docSendTargets) {
        this.docSendTargets = docSendTargets;
    }

    @Override
    public String toString() {
        return "DocArchiveVo{" +
                "docArchive=" + docArchive +
                ", docSend=" + docSend +
                ", docSendTargets=" + docSendTargets +
                '}';
    }
}
