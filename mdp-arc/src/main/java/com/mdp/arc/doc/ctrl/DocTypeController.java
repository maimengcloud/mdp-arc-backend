package com.mdp.arc.doc.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.doc.entity.DocType;
import com.mdp.arc.doc.service.DocTypeService;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="/*/doc/docType")
@Api(tags={"文档种类-作废-改为字典doc_type-操作接口"})
public class DocTypeController {
	
	static Logger logger =LoggerFactory.getLogger(DocTypeController.class);
	
	@Autowired
	private DocTypeService docTypeService;

	@ApiOperation( value = "文档种类-作废-改为字典doc_type-查询列表",notes=" ")
	@ApiEntityParams(DocType.class)
	@ApiResponses({
		@ApiResponse(code = 200,response=DocType.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listDocType(@ApiIgnore @RequestParam Map<String,Object> params){
		try {
			User user=LoginUtils.getCurrentUserInfo();
			QueryWrapper<DocType> qw = QueryTools.initQueryWrapper(DocType.class , params);
			IPage page=QueryTools.initPage(params);
			List<Map<String,Object>> datas = docTypeService.selectListMapByWhere(page,qw,params);
			return Result.ok("query-ok","查询成功").setData(datas).setTotal(page.getTotal());
		}catch (BizException e) {
			return Result.error(e);
		}catch (Exception e) {
			return Result.error(e);
		}
	}
	

	@ApiOperation( value = "文档种类-作废-改为字典doc_type-新增",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocType.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addDocType(@RequestBody DocType docType) {
		 docTypeService.save(docType);
         return Result.ok("add-ok","添加成功！");
	}

	@ApiOperation( value = "文档种类-作废-改为字典doc_type-删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delDocType(@RequestBody DocType docType){
		docTypeService.removeById(docType);
        return Result.ok("del-ok","删除成功！");
	}

	@ApiOperation( value = "文档种类-作废-改为字典doc_type-修改",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=DocType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editDocType(@RequestBody DocType docType) {
		docTypeService.updateById(docType);
        return Result.ok("edit-ok","修改成功！");
	}

    @ApiOperation( value = "文档种类-作废-改为字典doc_type-批量修改某些字段",notes="")
    @ApiEntityParams( value = DocType.class, props={ }, remark = "文档种类-作废-改为字典doc_type", paramType = "body" )
	@ApiResponses({
			@ApiResponse(code = 200,response=DocType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/editSomeFields",method=RequestMethod.POST)
	public Result editSomeFields( @ApiIgnore @RequestBody Map<String,Object> params) {
        try{
            User user= LoginUtils.getCurrentUserInfo();
            docTypeService.editSomeFields(params);
            return Result.ok("edit-ok","更新成功");
        }catch (BizException e) {
            logger.error("",e);
            return Result.error(e);
        }catch (Exception e) {
            logger.error("",e);
            return Result.error(e);
        }
	}

	@ApiOperation( value = "文档种类-作废-改为字典doc_type-批量删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelDocType(@RequestBody List<DocType> docTypes) {
	    User user= LoginUtils.getCurrentUserInfo();
        try{ 
            if(docTypes.size()<=0){
                return Result.error("docType-batchDel-data-err-0","请上送待删除数据列表");
            }
             List<DocType> datasDb=docTypeService.listByIds(docTypes.stream().map(i-> i.getTypeId() ).collect(Collectors.toList()));

            List<DocType> can=new ArrayList<>();
            List<DocType> no=new ArrayList<>();
            for (DocType data : datasDb) {
                if(true){
                    can.add(data);
                }else{
                    no.add(data);
                } 
            }
            List<String> msgs=new ArrayList<>();
            if(can.size()>0){
                docTypeService.removeByIds(can);
                msgs.add(LangTips.transMsg("del-ok-num","成功删除%s条数据.",can.size()));
            }
    
            if(no.size()>0){ 
                msgs.add(LangTips.transMsg("not-allow-del-num","以下%s条数据不能删除:【%s】",no.size(),no.stream().map(i-> i.getTypeId() ).collect(Collectors.joining(","))));
            }
            if(can.size()>0){
                 return Result.ok(msgs.stream().collect(Collectors.joining()));
            }else {
                return Result.error(msgs.stream().collect(Collectors.joining()));
            } 
        }catch (BizException e) { 
           return Result.error(e);
        }catch (Exception e) {
            return Result.error(e);
        }


	} 

	@ApiOperation( value = "文档种类-作废-改为字典doc_type-根据主键查询一条数据",notes=" ")
     @ApiResponses({
            @ApiResponse(code = 200,response=DocType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value="/queryById",method=RequestMethod.GET)
    public Result queryById(DocType docType) {
        DocType data = (DocType) docTypeService.getById(docType);
        return Result.ok().setData(data);
    }

	/**
	 * 查询分类并统计关联的公文数量
	 * @param docType
	 * @return
	 */
	@RequestMapping(value="/listAndCountType",method=RequestMethod.GET)
	public Map<String,Object> listAndCountType( @RequestParam Map<String,Object> docType){
		List<Map<String,Object>> datas = docTypeService.selectListAndCountType(docType);	//列出DocType列表
		return Result.ok().setData(datas).put("count",datas.size());
	}

}
