package  com.mdp.arc.doc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 doc  小模块 DocSendTarget<br>
 */
@Data
@TableName("arc_doc_send_target")
@ApiModel(description="发文对象")
public class DocSendTarget  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="对象类型user用户/dept部门",allowEmptyValue=true,example="",allowableValues="")
	String targetType;

	
	@ApiModelProperty(notes="对象编号",allowEmptyValue=true,example="",allowableValues="")
	String targetId;

	
	@ApiModelProperty(notes="对象名称",allowEmptyValue=true,example="",allowableValues="")
	String targetName;

	
	@ApiModelProperty(notes="read读/edit编辑/full完全控制",allowEmptyValue=true,example="",allowableValues="")
	String authority;

	
	@ApiModelProperty(notes="是否可下载",allowEmptyValue=true,example="",allowableValues="")
	String canDownload;

	
	@ApiModelProperty(notes="是否已接收",allowEmptyValue=true,example="",allowableValues="")
	String isReceive;

	
	@ApiModelProperty(notes="接收时间",allowEmptyValue=true,example="",allowableValues="")
	Date receiveDate;

	
	@ApiModelProperty(notes="接收部门",allowEmptyValue=true,example="",allowableValues="")
	String receiveDeptid;

	
	@ApiModelProperty(notes="接收人编号",allowEmptyValue=true,example="",allowableValues="")
	String receiveUserid;

	
	@ApiModelProperty(notes="接收人姓名",allowEmptyValue=true,example="",allowableValues="")
	String receiveUsername;

	
	@ApiModelProperty(notes="接收部门名称",allowEmptyValue=true,example="",allowableValues="")
	String receiveDeptName;

	
	@ApiModelProperty(notes="文档编号",allowEmptyValue=true,example="",allowableValues="")
	String docNo;

	
	@ApiModelProperty(notes="main主送/carbon抄送",allowEmptyValue=true,example="",allowableValues="")
	String sendType;

	
	@ApiModelProperty(notes="对象归属机构号",allowEmptyValue=true,example="",allowableValues="")
	String targetBranchId;

	
	@ApiModelProperty(notes="主键",allowEmptyValue=true,example="",allowableValues="")
	String docId;

	/**
	 *主键
	 **/
	public DocSendTarget(String id) {
		this.id = id;
	}
    
    /**
     * 发文对象
     **/
	public DocSendTarget() {
	}

}