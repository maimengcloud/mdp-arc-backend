package com.mdp.arc.archive.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组织 com.qqkj  顶级模块 mdp 大模块 arc  小模块 <br> 
 * 实体 ArchiveCategory所有属性名: <br>
 *	id,archiveId,categoryId;<br>
 * 表 ARC.arc_archive_category 文章分类关系表的所有字段名: <br>
 *	id,archive_id,category_id;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="文章分类关系")
public class ArchiveCategoryVo  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	  

	@ApiModelProperty(notes="文章编号",allowEmptyValue=true,example="",allowableValues="")
	String archiveId;
	
	@ApiModelProperty(notes="分类编号列表",allowEmptyValue=true,example="",allowableValues="")
	String[] categoryIds;
 
	public String getArchiveId() {
		return archiveId;
	}

	public void setArchiveId(String archiveId) {
		this.archiveId = archiveId;
	}
	

	public String[] getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(String ... categoryIds) {
		this.categoryIds = categoryIds;
	}
}