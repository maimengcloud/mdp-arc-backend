package com.mdp.arc.archive.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 archive  小模块 <br> 
 * 实体 ArchiveUp所有属性名: <br>
 *	"archiveId","文章编号","userid","点赞人员编号","utime","点赞时间";<br>
 * 当前主键(包括多主键):<br>
 *	archive_id,userid;<br>
 */
 @Data
@ApiModel(description="文章阅读数量登记")
public class UpReadNumsVo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes="文章编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String archiveId;

	@ApiModelProperty(notes="阅读数量",allowEmptyValue=true,example="",allowableValues="")
	int nums;


}