package  com.mdp.arc.archive.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 archive  小模块 ArchiveUp<br>
 */
@Data
@TableName("arc_archive_up")
@ApiModel(description="文章点赞人员列表")
public class ArchiveUp  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="文章编号,主键",allowEmptyValue=true,example="",allowableValues="")
    String archiveId;
    @TableIds
	
    @ApiModelProperty(notes="点赞人员编号,主键",allowEmptyValue=true,example="",allowableValues="")
    String userid;

	
	@ApiModelProperty(notes="点赞时间",allowEmptyValue=true,example="",allowableValues="")
	Date utime;

	/**
	 *文章编号,点赞人员编号
	 **/
	public ArchiveUp(String archiveId,String userid) {
		this.archiveId = archiveId;
		this.userid = userid;
	}
    
    /**
     * 文章点赞人员列表
     **/
	public ArchiveUp() {
	}

}