package  com.mdp.arc.archive.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 archive  小模块 ArchiveCategory<br>
 */
@Data
@TableName("arc_archive_category")
@ApiModel(description="文章分类关系表")
public class ArchiveCategory  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="文章编号,主键",allowEmptyValue=true,example="",allowableValues="")
    String archiveId;
    @TableIds
	
    @ApiModelProperty(notes="分类编号,主键",allowEmptyValue=true,example="",allowableValues="")
    String categoryId;

	/**
	 *文章编号,分类编号
	 **/
	public ArchiveCategory(String archiveId,String categoryId) {
		this.archiveId = archiveId;
		this.categoryId = categoryId;
	}
    
    /**
     * 文章分类关系表
     **/
	public ArchiveCategory() {
	}

}