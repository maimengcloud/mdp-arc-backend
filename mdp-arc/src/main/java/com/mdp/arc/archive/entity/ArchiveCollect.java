package  com.mdp.arc.archive.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 arc 大模块 archive  小模块 ArchiveCollect<br>
 */
@Data
@TableName("arc_archive_collect")
@ApiModel(description="文章收藏人员列表")
public class ArchiveCollect  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="文章编号,主键",allowEmptyValue=true,example="",allowableValues="")
    String archiveId;
    @TableIds
	
    @ApiModelProperty(notes="收藏人员编号,主键",allowEmptyValue=true,example="",allowableValues="")
    String userid;

	
	@ApiModelProperty(notes="收藏时间",allowEmptyValue=true,example="",allowableValues="")
	Date ctime;

	/**
	 *文章编号,收藏人员编号
	 **/
	public ArchiveCollect(String archiveId,String userid) {
		this.archiveId = archiveId;
		this.userid = userid;
	}
    
    /**
     * 文章收藏人员列表
     **/
	public ArchiveCollect() {
	}

}