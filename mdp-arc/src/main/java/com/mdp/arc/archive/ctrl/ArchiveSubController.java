package com.mdp.arc.archive.ctrl;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 为了兼容前端部分接口带有arc/archive/archive前缀的
 ***/
@RestController("mdp.arc.ArchiveSubController")
@RequestMapping(value="/*/arc/archive/archive")
@Deprecated
public class ArchiveSubController  extends ArchiveController{

}
