package com.mdp.arc.archive.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.archive.entity.ArchiveCategory;
import com.mdp.arc.archive.service.ArchiveCategoryService;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.swagger.ApiEntityParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * url编制采用rest风格,如对ARC.arc_archive_category 文章分类关系表的操作有增删改查,对应的url分别为:<br>
 *  新增: arc/archiveCategory/add <br>
 *  查询: arc/archiveCategory/list<br>
 *  模糊查询: arc/archiveCategory/listKey<br>
 *  修改: arc/archiveCategory/edit <br>
 *  删除: arc/archiveCategory/del<br>
 *  批量删除: arc/archiveCategory/batchDel<br>
 * 组织 com.qqkj  顶级模块 mdp 大模块 arc 小模块 <br>
 * 实体 ArchiveCategory 表 ARC.arc_archive_category 当前主键(包括多主键): id; 
 ***/
@RestController("mdp.arc.archiveCategoryController")
@RequestMapping(value="/*/arc/archive/archiveCategory")
@Api(tags={"文章分类关系表操作接口"})
public class ArchiveCategoryController {
	
	static Log logger=LogFactory.getLog(ArchiveCategoryController.class);
	
	@Autowired
	private ArchiveCategoryService archiveCategoryService;
	 
		 
	
	@ApiOperation( value = "查询文章分类关系表信息列表",notes="listArchiveCategory,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
	@ApiEntityParams(ArchiveCategory.class)
	@ApiResponses({
		@ApiResponse(code = 200,response= ArchiveCategory.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listArchiveCategory( @RequestParam Map<String,Object> params ){

		IPage page= QueryTools.initPage(params);
		List<Map<String,Object>>	datas = archiveCategoryService.selectListMapByWhere(page,QueryTools.initQueryWrapper(ArchiveCategory.class,params),params);	//列出ArchiveCategory列表
		return Result.ok().setData(datas).setTotal(page.getTotal());
	}
	
 
	
	/**
	@ApiOperation( value = "新增一条文章分类关系表信息",notes="addArchiveCategory,主键如果为空，后台自动生成")
	@ApiResponses({
		@ApiResponse(code = 200,response=ArchiveCategory.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addArchiveCategory(@RequestBody ArchiveCategory archiveCategory) {
		
		
		try{
			archiveCategory.setId(archiveCategoryService.createKey("id"));
			archiveCategoryService.insert(archiveCategory);
			return Result.ok().setData(archiveCategory);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}  
		return Result.ok();
	}
	*/
	
	/**
	@ApiOperation( value = "删除一条文章分类关系表信息",notes="delArchiveCategory,仅需要上传主键字段")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delArchiveCategory(@RequestBody ArchiveCategory archiveCategory){
		
		
		try{
			archiveCategoryService.deleteByPk(archiveCategory);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}  
		return Result.ok();
	}
	 */
	
	/**
	@ApiOperation( value = "根据主键修改一条文章分类关系表信息",notes="editArchiveCategory")
	@ApiResponses({
		@ApiResponse(code = 200,response=ArchiveCategory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editArchiveCategory(@RequestBody ArchiveCategory archiveCategory) {
		
		
		try{
			archiveCategoryService.updateByPk(archiveCategory);
			return Result.ok().setData(archiveCategory);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}  
		return Result.ok();
	}
	*/
	

	
	/**
	@ApiOperation( value = "根据主键列表批量删除文章分类关系表信息",notes="batchDelArchiveCategory,仅需要上传主键字段")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelArchiveCategory(@RequestBody List<ArchiveCategory> archiveCategorys) {
		
		
		try{ 
			archiveCategoryService.batchDelete(archiveCategorys);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}  
		return Result.ok();
	} 
	*/
}
