package com.mdp.arc.archive.ctrl;

import com.mdp.arc.archive.entity.Archive;
import com.mdp.arc.archive.entity.UserForumCpdVo;
import com.mdp.arc.archive.service.ArchiveService;
import com.mdp.arc.pub.service.CategoryService;
import com.mdp.core.entity.Result;
import com.mdp.core.utils.BaseUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import io.swagger.annotations.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

/**
 * url编制采用rest风格,如对ARC.arc_archive的操作有增删改查,对应的url分别为:<br>
 *  新增: arc/archive/add <br>
 *  查询: arc/archive/list<br>
 *  模糊查询: arc/archive/listKey<br>
 *  修改: arc/archive/edit <br>
 *  删除: arc/archive/del<br>
 *  批量删除: arc/archive/batchDel<br>
 * 组织 com.qqkj  顶级模块 mdp 大模块 arc 小模块 <br>
 * 实体 Archive 表 ARC.arc_archive 当前主键(包括多主键): id;
 ***/
@RestController("mdp.arc.archiveForumCpdController")
@RequestMapping(value="/*/arc/archive/forum")
public class ArchiveForumCpdController {

	static Log logger=LogFactory.getLog(ArchiveForumCpdController.class);

	@Autowired
	private ArchiveService archiveService;


	@Autowired
	private CategoryService categoryService;

	

	Map<String,Object> fieldsMap= BaseUtils.toMap(new Archive());



	@Value("${mdp.platform-branch-id:platform-branch-001}")
	String platformBranchId;


	@ApiOperation( value = "查询某个人的贴子数，获赞数",notes=" ")
	@ApiImplicitParams({
			@ApiImplicitParam(name="userid",value="用户编号，空值默认为当前登录账户",required=false),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response= UserForumCpdVo.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/userForumCpd",method=RequestMethod.GET)
	public Result myCollect(@ApiIgnore @RequestParam Map<String,Object> params ){
		
		String userid= params!=null? (String) params.get("userid"):null;
		if(params==null || !StringUtils.hasText(userid)){
			User user=LoginUtils.getCurrentUserInfo();
			userid=user.getUserid();
		}
		if(!StringUtils.hasText(userid)){
			return Result.error("userid-0","用户编号不能为空");
		}
		UserForumCpdVo UserForumCpdVo=this.archiveService.userForumCpd(userid);
		return Result.ok().setData(UserForumCpdVo);
		

	}




}
