package com.mdp.arc.archive.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.archive.entity.Archive;
import com.mdp.arc.archive.entity.UserForumCpdVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchiveMapper extends BaseMapper<Archive> {

    /**
     * 自定义查询，支持多表关联
     * @param page 分页条件
     * @param ew 一定要，并且必须加@Param("ew")注解
     * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    List<Map<String,Object>> selectListMapByWhere(IPage page, @Param("ew") QueryWrapper ew,@Param("ext") Map<String,Object> ext);

    void closeComment(List<String> ids);

    void openComment(List<String> ids);

    void updateTagsByArchiveId(Map<String, Object> p);

    void updateArchiveStaticById(Map<String, Object> map);

    List<Map<String, Object>> myCollect(Map<String, Object> archive);

    UserForumCpdVo userForumCpd(String userid);
 

    void updateUpNums(List<String> archiveIds);

    void updateCommentNums(List<String> archiveIds);

    void updateCollectNums(List<String> archiveIds);

    List<Map<String, Object>> selectFrinedMessage(IPage page, Map<String, Object> params);


    void upReadNums(Map<String, Object> map);
}

