package com.mdp.arc.archive.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.archive.entity.Archive;
import com.mdp.arc.archive.service.ArchiveCalcService;
import com.mdp.arc.archive.service.ArchiveService;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.utils.LoginUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * url编制采用rest风格,如对ARC.arc_archive的操作有增删改查,对应的url分别为:<br>
 *  新增: arc/archive/add <br>
 *  查询: arc/archive/list<br>
 *  模糊查询: arc/archive/listKey<br>
 *  修改: arc/archive/edit <br>
 *  删除: arc/archive/del<br>
 *  批量删除: arc/archive/batchDel<br>
 * 组织 com.qqkj  顶级模块 mdp 大模块 arc 小模块 <br>
 * 实体 Archive 表 ARC.arc_archive 当前主键(包括多主键): id; 
 ***/
@Controller("mdp.arc.archiveShowController")
@RequestMapping(value="/*/arc/archive")
public class ArchiveShowController {
	
	static Log logger=LogFactory.getLog(ArchiveShowController.class);
	
	@Autowired
	private ArchiveService archiveService;

	@Value("${mdp.platform-branch-id:platform-branch-001}")
	String platformBranchId;
	 
	/**
	 * 直接显示文章
	 */
	@RequestMapping(value="/showArchive",method=RequestMethod.GET)
	public ModelAndView show(@RequestParam Map<String,Object> archive) {
		ModelAndView m = new ModelAndView(); 
		Tips tips=new Tips();
		try{ 
			if(archive.containsKey("id")) {
				Archive archiveObj = archiveService.selectOneObject( new Archive((String) archive.get("id")));	//列出Archive列表
				if(archiveObj==null) {
					tips.setErrMsg("文章不存在或者已经下架");
				}else {
					m.addObject("data", archiveObj);
					ArchiveCalcService.putReadNum(archiveObj.getId(),1);
				}
			}else {
				tips.setErrMsg("文章编号不能为空");
			}
			
			
		}catch (BizException e) { 
			logger.error("执行异常",e);
			tips=e.getTips();
		}catch (Exception e) {
			logger.error("执行异常",e);
			tips.setErrMsg(e.getMessage());
		}  
		m.addObject("tips", tips);
		m.setViewName("showArchive");
		return m;
	}
	@ApiOperation( value = "查询档案信息表信息列表",notes="listArchive,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")

	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/front/list",method=RequestMethod.GET)
	@ResponseBody
	public Result listArchive(@RequestParam Map<String,Object> params ){
		
		IPage page= QueryTools.initPage(params);
		RequestUtils.transformArray(params, "categoryIds");
		RequestUtils.transformArray(params, "tagIds");
		if(params.get("tagIds")!=null){
			params.put("tagIdList",params.get("tagIds"));
			params.remove("tagIds");
		}
		if(params.containsKey("eventKey")){
			params.put("categoryId",params.get("eventKey"));
		}
		String branchId= (String) params.get("branchId");
		if(!StringUtils.hasText(branchId)){
			branchId= LoginUtils.getCurrentUserInfo().getBranchId();
			params.put("branchId",branchId);
		}
		if(!platformBranchId.equals(branchId)){
			List<String> branchIds=new ArrayList<>();
			branchIds.add(branchId);
			branchIds.add(platformBranchId);
			params.put("branchIds",branchIds);
		}
		List<Map<String,Object>>	datas = archiveService.selectListMapByWhere(page,QueryTools.initQueryWrapper(Archive.class,params),params);	//列出Archive列表
		
		return Result.ok().setData(datas).setTotal(page.getTotal());
		

	}
}
