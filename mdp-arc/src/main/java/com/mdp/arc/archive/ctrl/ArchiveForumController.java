package com.mdp.arc.archive.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.archive.entity.Archive;
import com.mdp.arc.archive.entity.UpReadNumsVo;
import com.mdp.arc.archive.service.ArchiveCalcService;
import com.mdp.arc.archive.service.ArchiveService;
import com.mdp.arc.pub.service.CategoryService;
import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.BaseUtils;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import io.swagger.annotations.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

/**
 * url编制采用rest风格,如对ARC.arc_archive的操作有增删改查,对应的url分别为:<br>
 *  新增: arc/archive/add <br>
 *  查询: arc/archive/list<br>
 *  模糊查询: arc/archive/listKey<br>
 *  修改: arc/archive/edit <br>
 *  删除: arc/archive/del<br>
 *  批量删除: arc/archive/batchDel<br>
 * 组织 com.qqkj  顶级模块 mdp 大模块 arc 小模块 <br>
 * 实体 Archive 表 ARC.arc_archive 当前主键(包括多主键): id;
 ***/
@RestController("mdp.arc.archiveForumController")
@RequestMapping(value="/*/arc/archive/forum")
public class ArchiveForumController {

	static Log logger=LogFactory.getLog(ArchiveForumController.class);

	@Autowired
	private ArchiveService archiveService;


	@Autowired
	private CategoryService categoryService;

	

	Map<String,Object> fieldsMap= BaseUtils.toMap(new Archive());



	@Value("${mdp.platform-branch-id:platform-branch-001}")
	String platformBranchId;

	@ApiOperation( value = "查询文章列表",notes=" ")
	@ApiEntityParams( Archive.class )
	@ApiImplicitParams({
			@ApiImplicitParam(name="pageSize",value="每页大小，默认20条",required=false),
			@ApiImplicitParam(name="pageNum",value="当前页码,从1开始",required=false),
			@ApiImplicitParam(name="total",value="总记录数,服务器端收到0时，会自动计算总记录数，如果上传>0的不自动计算",required=false),
			@ApiImplicitParam(name="count",value="是否计算总记录条数，如果count=true,则计算计算总条数，如果count=false 则不计算",required=false),
			@ApiImplicitParam(name="orderBy",value="排序列 如性别、学生编号排序 orderBy = sex desc,student desc",required=false),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result list(@ApiIgnore @RequestParam Map<String,Object> params ){
		
		IPage page= QueryTools.initPage(params);
		RequestUtils.transformArray(params, "categoryIds");
		RequestUtils.transformArray(params, "tagIds");
		if(params.get("tagIds")!=null){
			params.put("tagIdList",params.get("tagIds"));
			params.remove("tagIds");
		}
		if(params.containsKey("eventKey")){
			params.put("categoryId",params.get("eventKey"));
		}
		params.put("archiveType","5");
		params.remove("branchId");
		params.putIfAbsent("status","1");
		List<Map<String,Object>>	datas = archiveService.selectListMapByWhere(page,QueryTools.initQueryWrapper(Archive.class,params),params);	//列出Archive列表
		
		return Result.ok().setData(datas).setTotal(page.getTotal());
		

	}

	@ApiOperation( value = "查询档案信息表信息明细",notes="detail")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="主键,主键",required=true),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/detail",method=RequestMethod.GET)
	public Result detail( @ApiIgnore @RequestParam Map<String,Object> params ){
		
		Archive archiveObj = archiveService.selectOneObject( new Archive((String) params.get("id")));	//列出Archive列表
		if(archiveObj!=null && StringUtils.hasText(archiveObj.getId())){
			ArchiveCalcService.putReadNum(archiveObj.getId(),1);
		}
		return Result.ok().setData(archiveObj);
		

	}


	/**
	 * 批量添加文章阅读数量
	 */
	@ApiOperation( value = "批量添加文章阅读数量",notes=" ")
	@ApiResponses({
			@ApiResponse(code = 200,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/upReadNums",method=RequestMethod.POST)
	public Result upReadNums(@RequestBody List<UpReadNumsVo> readNumsVos) {
		
		
		try{
			if(readNumsVos!=null && readNumsVos.size()>0){
				for (UpReadNumsVo readNumsVo : readNumsVos) {
					ArchiveCalcService.putReadNum(readNumsVo.getArchiveId(), readNumsVo.getNums());
				}
			}
			//return Result.ok().setData(archive);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}

	@ApiOperation( value = "查询我的收藏文章列表",notes=" ")
	@ApiEntityParams( Archive.class )
	@ApiImplicitParams({
			@ApiImplicitParam(name="pageSize",value="每页大小，默认20条",required=false),
			@ApiImplicitParam(name="pageNum",value="当前页码,从1开始",required=false),
			@ApiImplicitParam(name="total",value="总记录数,服务器端收到0时，会自动计算总记录数，如果上传>0的不自动计算",required=false),
			@ApiImplicitParam(name="count",value="是否计算总记录条数，如果count=true,则计算计算总条数，如果count=false 则不计算",required=false),
			@ApiImplicitParam(name="orderBy",value="排序列 如性别、学生编号排序 orderBy = sex desc,student desc",required=false),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/myCollect",method=RequestMethod.GET)
	public Result myCollect( @ApiIgnore @RequestParam Map<String,Object> params ){
		
		IPage page= QueryTools.initPage(params);
		RequestUtils.transformArray(params, "categoryIds");
		RequestUtils.transformArray(params, "tagIds");
		if(params.get("tagIds")!=null){
			params.put("tagIdList",params.get("tagIds"));
			params.remove("tagIds");
		}
		if(params.containsKey("eventKey")){
			params.put("categoryId",params.get("eventKey"));
		}
		User user=LoginUtils.getCurrentUserInfo();
		params.put("collectUserid",user.getUserid());
		List<Map<String,Object>>	datas = archiveService.selectListMapByWhere(page,QueryTools.initQueryWrapper(Archive.class,params),params);	//列出Archive列表
		
		return Result.ok().setData(datas).setTotal(page.getTotal());
		

	}




}
