package  com.mdp.arc.archive.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023-9-15
 */
@Data
@TableName("arc_archive")
@ApiModel(description="档案信息表")
public class Archive  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="标签名多个,分割",allowEmptyValue=true,example="",allowableValues="")
	String tagNames;

	
	@ApiModelProperty(notes="创建部门",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="创建人",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="是否共享",allowEmptyValue=true,example="",allowableValues="")
	String isShare;

	
	@ApiModelProperty(notes="访问路径",allowEmptyValue=true,example="",allowableValues="")
	String url;

	
	@ApiModelProperty(notes="摘要",allowEmptyValue=true,example="",allowableValues="")
	String archiveAbstract;

	
	@ApiModelProperty(notes="内容",allowEmptyValue=true,example="",allowableValues="")
	String archiveContext;

	
	@ApiModelProperty(notes="标题",allowEmptyValue=true,example="",allowableValues="")
	String archiveTitle;

	
	@ApiModelProperty(notes="归档人",allowEmptyValue=true,example="",allowableValues="")
	String archivingUserid;

	
	@ApiModelProperty(notes="是否来自归档",allowEmptyValue=true,example="",allowableValues="")
	String isFromArchiving;

	
	@ApiModelProperty(notes="归档日期",allowEmptyValue=true,example="",allowableValues="")
	Date archivingDate;

	
	@ApiModelProperty(notes="创建日期",allowEmptyValue=true,example="",allowableValues="")
	Date createDate;

	
	@ApiModelProperty(notes="作者编号",allowEmptyValue=true,example="",allowableValues="")
	String authorUserid;

	
	@ApiModelProperty(notes="作者名称",allowEmptyValue=true,example="",allowableValues="")
	String authorName;

	
	@ApiModelProperty(notes="是否可删除",allowEmptyValue=true,example="",allowableValues="")
	String canDel;

	
	@ApiModelProperty(notes="是否可修改",allowEmptyValue=true,example="",allowableValues="")
	String canEdit;

	
	@ApiModelProperty(notes="是否可读",allowEmptyValue=true,example="",allowableValues="")
	String canRead;

	
	@ApiModelProperty(notes="业务编号",allowEmptyValue=true,example="",allowableValues="")
	String bizKey;

	
	@ApiModelProperty(notes="加密方式0不加密1私钥2公钥",allowEmptyValue=true,example="",allowableValues="")
	String encryptType;

	
	@ApiModelProperty(notes="开放评论",allowEmptyValue=true,example="",allowableValues="")
	String canComment;

	
	@ApiModelProperty(notes="0草稿1已上架2已下架3审核中",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="是否存到硬盘",allowEmptyValue=true,example="",allowableValues="")
	String isStorageDir;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="标题图片链接",allowEmptyValue=true,example="",allowableValues="")
	String titleImgUrl;

	
	@ApiModelProperty(notes="movie:视频(ARCHIVE_CONTEXT,存视频地址)",allowEmptyValue=true,example="",allowableValues="")
	String type;

	
	@ApiModelProperty(notes="时长:如果是视频,记录视频时长",allowEmptyValue=true,example="",allowableValues="")
	Integer duration;

	
	@ApiModelProperty(notes="标签编号列表,逗号分隔",allowEmptyValue=true,example="",allowableValues="")
	String tagIds;

	
	@ApiModelProperty(notes="0|知识库",allowEmptyValue=true,example="",allowableValues="")
	String archiveType;

	
	@ApiModelProperty(notes="图文说明",allowEmptyValue=true,example="",allowableValues="")
	String imageUrls;

	
	@ApiModelProperty(notes="关联文章列表[{imgUrl:'',imgDesc:'',linkUrl:'跳转链接',ext:'扩展字段'}]",allowEmptyValue=true,example="",allowableValues="")
	String linkArchives;

	
	@ApiModelProperty(notes="阅读人数",allowEmptyValue=true,example="",allowableValues="")
	Integer readNums;

	
	@ApiModelProperty(notes="评论人数",allowEmptyValue=true,example="",allowableValues="")
	Integer commentNums;

	
	@ApiModelProperty(notes="关联广告",allowEmptyValue=true,example="",allowableValues="")
	String linkAds;

	
	@ApiModelProperty(notes="关联用户",allowEmptyValue=true,example="",allowableValues="")
	String linkUsers;

	
	@ApiModelProperty(notes="扩展字段",allowEmptyValue=true,example="",allowableValues="")
	String extInfos;

	
	@ApiModelProperty(notes="点赞的次数",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal praiseSum;

	
	@ApiModelProperty(notes="火热0否1是",allowEmptyValue=true,example="",allowableValues="")
	String isHot;

	
	@ApiModelProperty(notes="推荐0否1是",allowEmptyValue=true,example="",allowableValues="")
	String isMain;

	
	@ApiModelProperty(notes="最后更新日期",allowEmptyValue=true,example="",allowableValues="")
	Date opDate;

	
	@ApiModelProperty(notes="排序",allowEmptyValue=true,example="",allowableValues="")
	Integer seqNo;

	
	@ApiModelProperty(notes="上级业务编号",allowEmptyValue=true,example="",allowableValues="")
	String pbizKey;

	
	@ApiModelProperty(notes="创建日期",allowEmptyValue=true,example="",allowableValues="")
	Date ctime;

	
	@ApiModelProperty(notes="ip地址",allowEmptyValue=true,example="",allowableValues="")
	String ip;

	
	@ApiModelProperty(notes="城市编号",allowEmptyValue=true,example="",allowableValues="")
	String cityId;

	
	@ApiModelProperty(notes="城市名称",allowEmptyValue=true,example="",allowableValues="")
	String cityName;

	
	@ApiModelProperty(notes="点赞数",allowEmptyValue=true,example="",allowableValues="")
	Integer upNums;

	
	@ApiModelProperty(notes="收藏数",allowEmptyValue=true,example="",allowableValues="")
	Integer collectNums;

	
	@ApiModelProperty(notes="参考类型，开放式字段，1-开源社区，2-项目论坛,逗号",allowEmptyValue=true,example="",allowableValues="")
	String relyType;

	
	@ApiModelProperty(notes="参考编号,逗号",allowEmptyValue=true,example="",allowableValues="")
	String relyIds;

	
	@ApiModelProperty(notes="参考子类型,逗号",allowEmptyValue=true,example="",allowableValues="")
	String relyStype;

	
	@ApiModelProperty(notes="参考子编号,逗号",allowEmptyValue=true,example="",allowableValues="")
	String relySids;

	/**
	 *主键
	 **/
	public Archive(String id) {
		this.id = id;
	}
    
    /**
     * 档案信息表
     **/
	public Archive() {
	}

}