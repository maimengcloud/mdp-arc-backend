package com.mdp.arc.archive.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description="论坛点赞数、贴子数获取接口")
public class UserForumCpdVo {
    @ApiModelProperty(notes="贴子数",allowEmptyValue=true,example="",allowableValues="")
    int archives;

    @ApiModelProperty(notes="点赞数",allowEmptyValue=true,example="",allowableValues="")
    int ups;
}
