package com.mdp.arc.archive.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.archive.entity.ArchiveComment;
import com.mdp.arc.archive.mapper.ArchiveCommentMapper;
import com.mdp.core.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * 父类已经支持增删改查操作,因此,即使本类什么也不写,也已经可以满足一般的增删改查操作了.<br> 
 * 组织 com.mdp  顶级模块 arc 大模块 archive 小模块 <br>
 * 实体 ArchiveComment 表 arc_archive_comment 当前主键(包括多主键): id; 
 ***/
@Service
public class ArchiveCommentService extends BaseService<ArchiveCommentMapper,ArchiveComment> {
	static Logger logger =LoggerFactory.getLogger(ArchiveCommentService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}

    public List<Map<String, Object>> selectCommentListNoPcomment(IPage page, Map<String, Object> params) {
		return baseMapper.selectCommentListNoPcomment(page,params);
    }

	public List<Map<String, Object>> selectArchiveCommentLev(List<String> id) {
		return baseMapper.selectArchiveCommentLev(id);
	}

	public void updateChildrenSum(String pcommentId, Integer addCount) {
		baseMapper.updateChildrenSum(map("pcommentId",pcommentId,"addCount",addCount));
	}

	public void praiseComment(ArchiveComment archiveComment) {
		baseMapper.praiseComment(archiveComment);
	}

	public void unShowComment(String[] ids) {
		baseMapper.unShowComment(ids);
	}

	public void showComment(String[] ids) {
		baseMapper.showComment(ids);
	}
}

