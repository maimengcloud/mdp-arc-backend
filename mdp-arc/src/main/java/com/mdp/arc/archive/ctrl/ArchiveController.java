package com.mdp.arc.archive.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.archive.entity.Archive;
import com.mdp.arc.archive.entity.ArchiveAddVo;
import com.mdp.arc.archive.entity.ArchiveCategoryVo;
import com.mdp.arc.archive.service.ArchiveCalcService;
import com.mdp.arc.archive.service.ArchiveService;
import com.mdp.arc.pub.entity.Category;
import com.mdp.arc.pub.service.CategoryService;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.core.service.BaseService;
import com.mdp.core.utils.BaseUtils;
import com.mdp.core.utils.RequestUtils;
import com.mdp.msg.client.PushNotifyMsgService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import io.swagger.annotations.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.*;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.fromMap;

/**
 * url编制采用rest风格,如对ARC.arc_archive的操作有增删改查,对应的url分别为:<br>
 *  新增: arc/archive/add <br>
 *  查询: arc/archive/list<br>
 *  模糊查询: arc/archive/listKey<br>
 *  修改: arc/archive/edit <br>
 *  删除: arc/archive/del<br>
 *  批量删除: arc/archive/batchDel<br>
 * 组织 com.qqkj  顶级模块 mdp 大模块 arc 小模块 <br>
 * 实体 Archive 表 ARC.arc_archive 当前主键(包括多主键): id;
 ***/
@RestController("mdp.arc.archiveController")
@RequestMapping(value="/*/arc/archive")
public class ArchiveController {

	static Log logger=LogFactory.getLog(ArchiveController.class);

	@Autowired
	private ArchiveService archiveService;


	@Autowired
	private CategoryService categoryService;



	Map<String,Object> fieldsMap= BaseUtils.toMap(new Archive());

	@Autowired
	PushNotifyMsgService pushNotifyMsgService;

	@Value("${mdp.platform-branch-id:platform-branch-001}")
	String platformBranchId;

	@ApiOperation( value = "查询档案信息表信息列表",notes=" ")
	@ApiEntityParams( Archive.class )
	@ApiImplicitParams({
			@ApiImplicitParam(name="pageSize",value="每页大小，默认20条",required=false),
			@ApiImplicitParam(name="pageNum",value="当前页码,从1开始",required=false),
			@ApiImplicitParam(name="total",value="总记录数,服务器端收到0时，会自动计算总记录数，如果上传>0的不自动计算",required=false),
			@ApiImplicitParam(name="count",value="是否计算总记录条数，如果count=true,则计算计算总条数，如果count=false 则不计算",required=false),
			@ApiImplicitParam(name="orderBy",value="排序列 如性别、学生编号排序 orderBy = sex desc,student desc",required=false),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listArchive(@ApiIgnore @RequestParam Map<String,Object> params ){
		
		IPage page= QueryTools.initPage(params);
		RequestUtils.transformArray(params, "categoryIds");
		RequestUtils.transformArray(params, "tagIdList");
		if(params.containsKey("eventKey")){
			params.put("categoryId",params.get("eventKey"));
		}
		String branchId= (String) params.get("branchId");
		if(!StringUtils.hasText(branchId)){
			branchId=LoginUtils.getCurrentUserInfo().getBranchId();
			params.put("branchId",branchId);
		}
		if(!platformBranchId.equals(branchId)){
			List<String> branchIds=new ArrayList<>();
			branchIds.add(branchId);
			branchIds.add(platformBranchId);
			params.put("branchIds",branchIds);
		}
		List<Map<String,Object>>	datas = archiveService.selectListMapByWhere(page,QueryTools.initQueryWrapper(Archive.class,params),params);	//列出Archive列表
		
		return Result.ok().setData(datas).setTotal(page.getTotal()); 
	}

	@ApiOperation( value = "查询新闻、公告、通知、论坛文章等免登录接口",notes="listArchive,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="主键,主键",required=false),
			@ApiImplicitParam(name="archiveType",value="文章所属主题 0|知识库" +
					"1|新闻" +
					"2|内部公告" +
					"3|平台公告" +
					"5|论坛" +
					"6|公文" +
					"4|其它" +
					"7|归档" +
					"8|网站栏目",required=false),
			@ApiImplicitParam(name="categoryId",value="文章所属分类",required=false),
			@ApiImplicitParam(name="categoryIds",value="同时查询多个分类",required=false),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/listNews",method=RequestMethod.GET)
	public Result listNews( @RequestParam Map<String,Object> params ){
		
		String pageSize= (String) params.get("pageSize");
		if(StringUtils.hasText(pageSize)){
			if(Integer.valueOf(pageSize)>20){
				params.put("pageSize",20);
			}
		}else{
			params.put("pageSize",20);
		}
		IPage page= QueryTools.initPage(params);
		RequestUtils.transformArray(params, "categoryIds");
		RequestUtils.transformArray(params, "tagIds");
		if(params.containsKey("eventKey")){
			params.put("categoryId",params.get("eventKey"));
		}
		String archiveType= (String) params.get("archiveType");
		if(!StringUtils.hasText(archiveType)){
			params.put("archiveTypes",Arrays.asList("1","3"));
		} else if(!"1".equals(archiveType) && !"3".equals(archiveType)){
			params.remove("archiveType");
			params.put("archiveTypes",Arrays.asList("1","3"));
		}

		params.putIfAbsent("status","1");
		List<Map<String,Object>>	datas = archiveService.selectListMapByWhere(page,QueryTools.initQueryWrapper(Archive.class,params),params);	//列出Archive列表
		
		return Result.ok().setData(datas).setTotal(page.getTotal());
	}
	@ApiOperation( value = "查询档案信息表信息明细",notes="getOneArchive")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="主键,主键",required=false),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/queryById",method=RequestMethod.GET)
	public Result queryById( @RequestParam Map<String,Object> params ){
		
		Archive archiveObj = archiveService.getById((String) params.get("id"));	//列出Archive列表
		if(archiveObj!=null && StringUtils.hasText(archiveObj.getId())){
			ArchiveCalcService.putReadNum(archiveObj.getId(), 1);
		}
		return Result.ok().setData(archiveObj);
	}
	@ApiOperation( value = "查询档案信息表信息明细",notes="getOneArchive")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="主键,主键",required=false),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/one",method=RequestMethod.GET)
	public Result getOneArchive( @RequestParam Map<String,Object> params ){
 		return queryById(params);
	}
	@ApiOperation( value = "根据分类查询该分类下的第一条档案信息信息明细",notes="getOneArchiveByCategoryId")
	@ApiImplicitParams({
			@ApiImplicitParam(name="oneByCategoryId",value="分类编号",required=false),
	})
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/oneByCategoryId",method=RequestMethod.GET)
	public Result getOneArchiveByCategoryId( @RequestParam Map<String,Object> params ){
		
		
		String categoryId=(String) params.get("categoryId");
		if(StringUtils.isEmpty(categoryId)) {
			return Result.error("分类编号不能为空");
		}else {
			Archive archiveObj = archiveService.getOneArchiveByCategoryId(categoryId);
			if(archiveObj!=null && StringUtils.hasText(archiveObj.getId())){
				ArchiveCalcService.putReadNum(archiveObj.getId(), 1);
			}
			return Result.ok().setData(archiveObj);
		} 
	}


	/**
	 * 新增一条数据
	 */
	@ApiOperation( value = "新增一条档案信息表信息",notes=" ")
	@ApiResponses({
			@ApiResponse(code = 200,response=Archive.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addArchive(@RequestBody ArchiveAddVo archiveVo) {
		
		
		try{
			User user=LoginUtils.getCurrentUserInfo();
			if(StringUtils.hasText(archiveVo.getCategoryId())){
				Category categoryDb=this.categoryService.selectOneObject(new Category(archiveVo.getCategoryId()));
				if(categoryDb==null){
					return Result.error("category-is-null","该分类已不存在");
				}else{
					if("1".equals(categoryDb.getLimitType())){
						Map<String,Object> p= BaseService.map("categoryId",archiveVo.getCategoryId());
						List<Map<String,Object>> archiveDbs=  this.archiveService.selectListMapByWhere(QueryTools.initPage(p),QueryTools.initQueryWrapper(Archive.class,p),p);
						if(archiveDbs.size()>0){
							return Result.error("category-archive-exists","该分类只能添加一篇文章，当前已存在，不允许再添加");
						}
					}
					if(!user.getBranchId().equals(categoryDb.getBranchId())){
						return Result.error("category-branchId-0","您无权在该分类下添加文章");
					}

				}
			}
			Archive params=archiveVo.getArchive();
			params.setCanComment("1");
			if(!StringUtils.hasText(params.getStatus())){
				params.setStatus("1");
			}
			params.setUserid(user.getUserid());
			params.setCreateDate(new Date());
			params.setArchivingDate(new Date());
			if(StringUtils.isEmpty(params.getAuthorUserid())) {
				params.setAuthorUserid(user.getUserid());
				if(StringUtils.isEmpty(params.getAuthorName())) {
					params.setAuthorName(user.getUsername());
				}
			}
			if(StringUtils.isEmpty(params.getBranchId())) {
				params.setBranchId(user.getBranchId());
				params.setDeptid(user.getDeptid());
			}
			if(StringUtils.isEmpty(params.getArchiveAbstract())) {
				params.setArchiveAbstract(params.getArchiveTitle());
			}
			params.setCtime(new Date());
			params.setOpDate(new Date());
			int i=archiveService.addArchive(archiveVo);


			return Result.ok().setData(params);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
	}
	/**
	 * 新增一条数据
	 */
	@RequestMapping(value="/add/workflow",method=RequestMethod.POST)
	public Result addWorkflowArchive(@RequestBody Archive archive) {
		
		
		try{
			if(StringUtils.isEmpty(archive.getId())){
				return Result.error("archiveId","档案编号不能为空，必须由前端上送");
			}else{
				if(archiveService.countByWhere(archive)>0){
					return Result.error("archiveId01","档案编号已经存在，请不要重复归档");
				}else{
					archiveService.insert(archive);
				}
			}
			return Result.ok().setData(archive);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		} 
	}

	/**
	 * 根据主键删除1条数据
	 */
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delArchive(@RequestBody Archive archive){
		
		
		try{
			Archive archiveDb=this.archiveService.selectOneById(archive.getId());
			if(archiveDb==null){
				return Result.error("data-0","文章已不存在");
			}
			User user = LoginUtils.getCurrentUserInfo();
			if(!user.getUserid().equals(archiveDb.getAuthorUserid()) && !LoginUtils.isBranchAdmin(archiveDb.getBranchId())  && !LoginUtils.isSuperAdmin()){
				return Result.error("no-qx-del","你无权限删除");
			}
			archiveService.deleteByPk(archive);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}


	/**
	 * 根据主键修改一条数据

	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editArchive(@RequestBody Archive archive) {
		
		
		try{
			params.setOpDate(new Date());
			archiveService.updateByPk(archive);
			return Result.ok().setData(archive);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}
	 */

	@ApiOperation(value = "批量修改某些字段", notes = "")
	@ApiEntityParams(value = Archive.class, props = {}, remark = "档案信息表", paramType = "body")
	@ApiResponses({
			@ApiResponse(code = 200, response = Archive.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
	public Map<String, Object> editSomeFields(@ApiIgnore @RequestBody Map<String, Object> archiveMap) {
		Map<String, Object> m = new HashMap<>();
		Tips tips = new Tips("成功更新一条数据");
		try {
			List<String> ids = (List<String>) archiveMap.get(QueryTools.PKS);
			if (ids == null || ids.size() == 0) {
				return Result.error("ids-0", "ids不能为空");
			}

			Set<String> fields = new HashSet<>();
			fields.add("id");
			for (String fieldName : archiveMap.keySet()) {
				if (fields.contains(fieldName)) {
					return Result.error(fieldName + "-no-edit", fieldName + "不允许修改");
				}
			}
			Set<String> fieldKey = archiveMap.keySet().stream().filter(i -> fieldsMap.containsKey(i)).collect(Collectors.toSet());
			fieldKey = fieldKey.stream().filter(i -> !StringUtils.isEmpty(archiveMap.get(i))).collect(Collectors.toSet());

			if (fieldKey.size() <= 0) {
				return Result.error("fieldKey-0", "没有需要更新的字段");
			}
			Archive archive = fromMap(archiveMap, Archive.class);
			List<Archive> archivesDb = archiveService.selectListByIds(ids);
			if (archivesDb == null || archivesDb.size() == 0) {
				return Result.error("data-0", "记录已不存在");
			}
			List<Archive> can = new ArrayList<>();
			List<Archive> no = new ArrayList<>();
			User user = LoginUtils.getCurrentUserInfo();
			for (Archive archiveDb : archivesDb) {
				if(!user.getUserid().equals(archiveDb.getAuthorUserid()) && !LoginUtils.isBranchAdmin(archiveDb.getBranchId()) && !LoginUtils.isSuperAdmin()){

					no.add(archiveDb);
				} else {
					can.add(archiveDb);
				}
			}
			if (can.size() > 0) {
				archiveMap.put(QueryTools.PKS, can.stream().map(i -> i.getId()).collect(Collectors.toList()));
				archiveService.editSomeFields(archiveMap);
			}
			List<String> msgs = new ArrayList<>();
			if (can.size() > 0) {
				msgs.add(String.format("成功更新以下%s条数据", can.size()));
			}
			if (no.size() > 0) {
				msgs.add(String.format("以下%s个数据无权限更新", no.size()));
			}
			if (can.size() > 0) {
				return Result.ok(msgs.stream().collect(Collectors.joining()));
			} else {
				return Result.error(msgs.stream().collect(Collectors.joining()));
			}
			//return Result.ok().setData(xmMenu);
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
	}


	@ApiOperation(value = "根据主键列表批量删除档案信息表信息", notes = " ")
	@ApiResponses({
			@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	})
	@RequestMapping(value = "/batchDel", method = RequestMethod.POST)
	public Map<String, Object> batchDelArchive(@RequestBody List<Archive> ids) {
		Map<String, Object> m = new HashMap<>();
		Tips tips = new Tips("成功删除");
		try {
			if (ids.size() <= 0) {
				return Result.error("data-0", "请上送待删除数据列表");
			}
			List<Archive> datasDb = archiveService.selectListByIds(ids);

			List<Archive> can = new ArrayList<>();
			List<Archive> no = new ArrayList<>();
			User user=LoginUtils.getCurrentUserInfo();
			for (Archive data : datasDb) {
				if (user.getUserid().equals(data.getAuthorUserid()) || LoginUtils.isBranchAdmin(data.getBranchId()) || LoginUtils.isSuperAdmin()) {
					can.add(data);
				} else {
					no.add(data);
				}
			}
			List<String> msgs = new ArrayList<>();
			if (can.size() > 0) {
				archiveService.batchDelete(can);
				msgs.add(String.format("成功删除%s条数据.", can.size()));
			}

			if (no.size() > 0) {
				msgs.add(String.format("以下%s条数据不能删除.【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
			}
			if (can.size() > 0) {
				return Result.ok(msgs.stream().collect(Collectors.joining()));
			} else {
				return Result.error(msgs.stream().collect(Collectors.joining()));
			}
		}catch (BizException e) { 
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
	}


	/**
	 * 批量关闭评论
	 */
	@RequestMapping(value="/comment/close",method=RequestMethod.POST)
	public Result closeComment(@RequestBody List<String> ids) {
		
		
		try{
			User user=LoginUtils.getCurrentUserInfo();
			List<Archive> archivesDb=this.archiveService.selectListByIds(ids);
			for (Archive archive : archivesDb) {
				if (!user.getUserid().equals(archive.getAuthorUserid()) && !LoginUtils.isBranchAdmin(archive.getBranchId()) && !LoginUtils.isSuperAdmin()) {
					return Result.error("no-qx-edit","您无权限关闭文章评论。文章【"+archive.getArchiveTitle()+"】");
				}
			}
			archiveService.closeComment(ids);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}
	/**
	 * 批量关闭评论
	 */
	@RequestMapping(value="/comment/open",method=RequestMethod.POST)
	public Result openComment(@RequestBody List<String> ids) {
		
		
		try{
			User user=LoginUtils.getCurrentUserInfo();
			List<Archive> archivesDb=this.archiveService.selectListByIds(ids);
			for (Archive archive : archivesDb) {
				if (!user.getUserid().equals(archive.getAuthorUserid()) && !LoginUtils.isBranchAdmin(archive.getBranchId())  && !LoginUtils.isSuperAdmin()) {
					return Result.error("no-qx-edit","您无权限打开文章评论,文章【"+archive.getArchiveTitle()+"】");
				}
			}
			archiveService.openComment(ids);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}
	/**
	 * 批量发布
	 */
	@RequestMapping(value="/publish",method=RequestMethod.POST)
	public Result publishArchive(@RequestBody ArchiveCategoryVo ac) {
		
		
		try{
			User user=LoginUtils.getCurrentUserInfo();
			List<Category> categoriesDb=this.categoryService.selectListByIds(Arrays.asList(ac.getCategoryIds()));
			if(categoriesDb==null || categoriesDb.size()<=0){
				return Result.error("categorys-0","分类不存在");
			}else{

				for (Category category : categoriesDb) {
					if(!user.getBranchId().equals(category.getBranchId())){
						return Result.error("categorys-branchId-0","您无权发布文章到分类【"+category.getName()+"】");
					}
				}
			}
			Archive archiveDb=this.archiveService.selectOneById(ac.getArchiveId());
			if(archiveDb==null){
				return Result.error("data-0","文章已不存在");
			}
			if(!user.getUserid().equals(archiveDb.getAuthorUserid()) && !LoginUtils.isBranchAdmin(archiveDb.getBranchId()) && !LoginUtils.isSuperAdmin()){
				return Result.error("no-qx-del","你无权限发布");
			}
			archiveService.publishArchive(ac);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}

	@RequestMapping(value="/publishForProcessApprova",method=RequestMethod.POST)
	public Result publishForProcessApprova(@RequestBody Map<String,Object> flowVars) {
		
		
		try{
			archiveService.publishForProcessApprova(flowVars);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}
	/**
	 * 批量下架
	 */
	@RequestMapping(value="/unpublish",method=RequestMethod.POST)
	public Result unPublishArchive(@RequestBody List<String> ids) {
		
		
		try{
			User user=LoginUtils.getCurrentUserInfo();
			List<Archive> archivesDb=this.archiveService.selectListByIds(ids);
			for (Archive archive : archivesDb) {
				if (!user.getUserid().equals(archive.getAuthorUserid()) && !LoginUtils.isBranchAdmin(archive.getBranchId()) && !LoginUtils.isSuperAdmin()) {
					return Result.error("no-qx-edit","您无权取消发布文章【"+archive.getArchiveTitle()+"】");
				}
			}
			archiveService.unPublishArchive(ids);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}

	/**
	 * 得到朋友圈的消息
	 */
	@RequestMapping(value="/getFriendCircleMessage",method=RequestMethod.GET)
	public Result getFriendCircleMessage( @RequestParam Map<String,Object> params ){
		
		IPage page= QueryTools.initPage(params);
		List<Map<String,Object>> archiveList=archiveService.selectFrinedMessage(page,params);
		
		return Result.ok().setData(archiveList);
	}

	/**
	 * 得到视频的消息
	 */
	@RequestMapping(value="/getMovieList",method=RequestMethod.GET)
	public Result getMovieList( @RequestParam Map<String,Object> params ){
		
		IPage page= QueryTools.initPage(params);
		List<Map<String,Object>> datas=archiveService.selectListMapByWhere( page,QueryTools.initQueryWrapper(Archive.class,params),params);
		
		return Result.ok().setData(datas).setTotal(page.getTotal());
	}

	/**
	 * 新增一条数据
	 */
	@RequestMapping(value="/addArchiveVideo",method=RequestMethod.POST)
	public Result addArchiveVideo(@RequestBody Archive params) {
		
		
		try{
			params.setCreateDate(new Date());
			params.setArchivingDate(new Date());
			User user=LoginUtils.getCurrentUserInfo();
			if(StringUtils.isEmpty(params.getAuthorUserid())) {

				if(StringUtils.isEmpty(params.getAuthorName())) {
					params.setAuthorName(user.getUsername());
				}
				params.setAuthorUserid(user.getUserid());
			}
			if(StringUtils.isEmpty(params.getBranchId())) {
				params.setBranchId(user.getBranchId());
				params.setDeptid(user.getDeptid());
			}
			if(StringUtils.isEmpty(params.getArchiveAbstract())) {
				params.setArchiveAbstract(params.getArchiveTitle());
			}
			archiveService.insert(params);

//			ArchiveStati archiveStati = new ArchiveStati();
//			archiveStati.setArchiveId(params.getId());
//			archiveStati.setPraiseSum(0);
//			archiveStati.setReaderSum(0);
//			archiveStatiService.insert(archiveStati);//创建完,设置阅读为0,点赞为0

			return Result.ok().setData(params);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
	}
}
