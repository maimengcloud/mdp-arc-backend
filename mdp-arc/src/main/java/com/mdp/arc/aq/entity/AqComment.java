package  com.mdp.arc.aq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023-8-31
 */
@Data
@TableName("arc_aq_comment")
@ApiModel(description="问题评论表")
public class AqComment  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="评论人",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="评论人姓名",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="星级",allowEmptyValue=true,example="",allowableValues="")
	String star;

	
	@ApiModelProperty(notes="时间",allowEmptyValue=true,example="",allowableValues="")
	Date createDate;

	
	@ApiModelProperty(notes="问题编号",allowEmptyValue=true,example="",allowableValues="")
	String questionId;

	
	@ApiModelProperty(notes="上级评论",allowEmptyValue=true,example="",allowableValues="")
	String pcommentId;

	
	@ApiModelProperty(notes="点赞数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal praiseSum;

	
	@ApiModelProperty(notes="是否显示0否1是",allowEmptyValue=true,example="",allowableValues="")
	String isShow;

	
	@ApiModelProperty(notes="接收本条评论的用户编号",allowEmptyValue=true,example="",allowableValues="")
	String toUserid;

	
	@ApiModelProperty(notes="接收本条评论的用户名称",allowEmptyValue=true,example="",allowableValues="")
	String toUsername;

	
	@ApiModelProperty(notes="层级0,1,2,3,4",allowEmptyValue=true,example="",allowableValues="")
	String lvl;

	
	@ApiModelProperty(notes="部门",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="评论内容",allowEmptyValue=true,example="",allowableValues="")
	String context;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public AqComment(String id) {
		this.id = id;
	}
    
    /**
     * 问题评论表
     **/
	public AqComment() {
	}

}