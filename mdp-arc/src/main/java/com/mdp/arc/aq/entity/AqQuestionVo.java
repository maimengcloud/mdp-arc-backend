package com.mdp.arc.aq.entity;

import java.sql.Date;

public class AqQuestionVo extends AqQuestion {
String categoryName;
String context;
Date createDate;
public String getCategoryName() {
	return categoryName;
}

public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}

public String getContext() {
	return context;
}

public void setContext(String context) {
	this.context = context;
}

public Date getCreateDate() {
	return createDate;
}

public void setCreateDate(Date createDate) {
	this.createDate = createDate;
}

}
