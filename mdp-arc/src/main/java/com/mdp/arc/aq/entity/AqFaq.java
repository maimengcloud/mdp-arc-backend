package  com.mdp.arc.aq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023-8-31
 */
@Data
@TableName("arc_aq_faq")
@ApiModel(description="常见问题表")
public class AqFaq  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="标题",allowEmptyValue=true,example="",allowableValues="")
	String title;

	
	@ApiModelProperty(notes="分类",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="回复文章",allowEmptyValue=true,example="",allowableValues="")
	String replyArchiveId;

	
	@ApiModelProperty(notes="回复链接",allowEmptyValue=true,example="",allowableValues="")
	String replyUrl;

	
	@ApiModelProperty(notes="是否显示",allowEmptyValue=true,example="",allowableValues="")
	String isShow;

	
	@ApiModelProperty(notes="回复内容",allowEmptyValue=true,example="",allowableValues="")
	String replyContext;

	
	@ApiModelProperty(notes="问询内容",allowEmptyValue=true,example="",allowableValues="")
	String askContext;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public AqFaq(String id) {
		this.id = id;
	}
    
    /**
     * 常见问题表
     **/
	public AqFaq() {
	}

}