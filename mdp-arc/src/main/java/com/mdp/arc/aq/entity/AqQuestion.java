package  com.mdp.arc.aq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023-8-31
 */
@Data
@TableName("arc_aq_question")
@ApiModel(description="问题表")
public class AqQuestion  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="标题",allowEmptyValue=true,example="",allowableValues="")
	String title;

	
	@ApiModelProperty(notes="分类",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="咨询人员编号",allowEmptyValue=true,example="",allowableValues="")
	String askUserid;

	
	@ApiModelProperty(notes="咨询人员手机号",allowEmptyValue=true,example="",allowableValues="")
	String askPhone;

	
	@ApiModelProperty(notes="咨询人员名称",allowEmptyValue=true,example="",allowableValues="")
	String askUsername;

	
	@ApiModelProperty(notes="状态0询问中1已回复2已结束",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="是否常见问题",allowEmptyValue=true,example="",allowableValues="")
	String isFaq;

	
	@ApiModelProperty(notes="询问部门",allowEmptyValue=true,example="",allowableValues="")
	String askDeptid;

	
	@ApiModelProperty(notes="咨询时间",allowEmptyValue=true,example="",allowableValues="")
	Date askDate;

	
	@ApiModelProperty(notes="问题内容",allowEmptyValue=true,example="",allowableValues="")
	String askContext;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public AqQuestion(String id) {
		this.id = id;
	}
    
    /**
     * 问题表
     **/
	public AqQuestion() {
	}

}