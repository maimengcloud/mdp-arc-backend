package com.mdp.arc.aq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.arc.aq.entity.AqComment;
import com.mdp.arc.aq.service.AqCommentService;
import com.mdp.arc.archive.entity.ArchiveComment;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
/**
 * @author maimeng-mdp code-gen
 * @since 2023-8-31
 */
@RestController
@RequestMapping(value="/*/arc/aq/aqComment")
@Api(tags={"问题评论表-操作接口"})
public class AqCommentController {
	
	static Logger logger =LoggerFactory.getLogger(AqCommentController.class);
	
	@Autowired
	private AqCommentService aqCommentService;

	@ApiOperation( value = "问题评论表-查询列表",notes=" ")
	@ApiEntityParams(AqComment.class)
	@ApiResponses({
		@ApiResponse(code = 200,response=AqComment.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listAqComment(@ApiIgnore @RequestParam Map<String,Object> params){
		try {
			User user=LoginUtils.getCurrentUserInfo();
			QueryWrapper<AqComment> qw = QueryTools.initQueryWrapper(AqComment.class , params);
			IPage page=QueryTools.initPage(params);
			List<Map<String,Object>> datas = aqCommentService.selectListMapByWhere(page,qw,params);
			return Result.ok("query-ok","查询成功").setData(datas).setTotal(page.getTotal());
		}catch (BizException e) {
			return Result.error(e);
		}catch (Exception e) {
			return Result.error(e);
		}
	}
	

	@ApiOperation( value = "问题评论表-新增",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=AqComment.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addAqComment(@RequestBody AqComment aqComment) {
		 aqCommentService.save(aqComment);
         return Result.ok("add-ok","添加成功！");
	}

	@ApiOperation( value = "问题评论表-删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delAqComment(@RequestBody AqComment aqComment){
		aqCommentService.removeById(aqComment);
        return Result.ok("del-ok","删除成功！");
	}

	@ApiOperation( value = "问题评论表-修改",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=AqComment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editAqComment(@RequestBody AqComment aqComment) {
		aqCommentService.updateById(aqComment);
        return Result.ok("edit-ok","修改成功！");
	}

    @ApiOperation( value = "问题评论表-批量修改某些字段",notes="")
    @ApiEntityParams( value = AqComment.class, props={ }, remark = "问题评论表", paramType = "body" )
	@ApiResponses({
			@ApiResponse(code = 200,response=AqComment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/editSomeFields",method=RequestMethod.POST)
	public Result editSomeFields( @ApiIgnore @RequestBody Map<String,Object> params) {
        try{
            User user= LoginUtils.getCurrentUserInfo();
            aqCommentService.editSomeFields(params);
            return Result.ok("edit-ok","更新成功");
        }catch (BizException e) {
            logger.error("",e);
            return Result.error(e);
        }catch (Exception e) {
            logger.error("",e);
            return Result.error(e);
        }
	}

	@ApiOperation( value = "问题评论表-批量删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelAqComment(@RequestBody List<AqComment> aqComments) {
	    User user= LoginUtils.getCurrentUserInfo();
        try{ 
            if(aqComments.size()<=0){
                return Result.error("aqComment-batchDel-data-err-0","请上送待删除数据列表");
            }
             List<AqComment> datasDb=aqCommentService.listByIds(aqComments.stream().map(i-> i.getId() ).collect(Collectors.toList()));

            List<AqComment> can=new ArrayList<>();
            List<AqComment> no=new ArrayList<>();
            for (AqComment data : datasDb) {
                if(true){
                    can.add(data);
                }else{
                    no.add(data);
                } 
            }
            List<String> msgs=new ArrayList<>();
            if(can.size()>0){
                aqCommentService.removeByIds(can);
                msgs.add(LangTips.transMsg("del-ok-num","成功删除%s条数据.",can.size()));
            }
    
            if(no.size()>0){ 
                msgs.add(LangTips.transMsg("not-allow-del-num","以下%s条数据不能删除:【%s】",no.size(),no.stream().map(i-> i.getId() ).collect(Collectors.joining(","))));
            }
            if(can.size()>0){
                 return Result.ok(msgs.stream().collect(Collectors.joining()));
            }else {
                return Result.error(msgs.stream().collect(Collectors.joining()));
            } 
        }catch (BizException e) { 
           return Result.error(e);
        }catch (Exception e) {
            return Result.error(e);
        }


	} 

	@ApiOperation( value = "问题评论表-根据主键查询一条数据",notes=" ")
     @ApiResponses({
            @ApiResponse(code = 200,response=AqComment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value="/queryById",method=RequestMethod.GET)
    public Result queryById(AqComment aqComment) {
        AqComment data = (AqComment) aqCommentService.getById(aqComment);
        return Result.ok().setData(data);
    }

	@ApiOperation( value = "点赞评论",notes="praiseComment")
	@ApiResponses({
			@ApiResponse(code = 200,response= ArchiveComment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/praise",method=RequestMethod.POST)
	public Result praiseComment(@RequestBody ArchiveComment archiveComment) {


		try{
			aqCommentService.praiseComment(archiveComment);
			return Result.ok().setData(archiveComment);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
	}

	@ApiOperation( value = "屏蔽评论",notes="unShowComment")
	@ApiResponses({
			@ApiResponse(code = 200,response=ArchiveComment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/unshow",method=RequestMethod.POST)
	public Result unShowComment(@RequestBody String[] ids) {


		try{
			aqCommentService.unShowComment(ids);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}

	@ApiOperation( value = "打开评论",notes="showComment")
	@ApiResponses({
			@ApiResponse(code = 200,response= ArchiveComment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/show",method=RequestMethod.POST)
	public Result showComment(@RequestBody String[] ids) {


		try{
			aqCommentService.showComment(ids);
		}catch (BizException e) {
			logger.error("执行异常",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("执行异常",e);
			return Result.error(e.getMessage());
		}
		return Result.ok();
	}
}
